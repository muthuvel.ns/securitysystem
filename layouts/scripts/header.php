<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Admin Template</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/pages/dashboard.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script> 
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- Notification -->

 <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/notification_menu/css/style_light.css">

    <script src="<?php echo base_url();?>assets/notification_menu/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/notification_menu/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/notification_menu/demo.js" type="text/javascript"></script>
-->

<!-- Notification End -->


</head>
<body>

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">

    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.html">Admin Template </a>
      <div class="nav-collapse">
<!--ul class="ttw-notification-menu">

    <li id="tasks" class="notification-menu-item"><a href="#">Notification</a></li>
<!--span title="Notifications" class="notification-bubble" style="background-color: rgb(254, 193, 81); display: inline;">0</span-->
<!--/ul-->
        <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> EGrappler.com <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Profile</a></li>
              <li><a href="javascript:;">Logout</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li class="active"><a href="index.html"><div class="text-center"><i class="icon-dashboard" id="menuicons"></i></br><span>Home</span></div></a> </li>
        <li><a href=""><div class="text-center"><i class="icon-code" id="menuicons"></i></br><span>Device Details</span></div> </a> </li>
        <li><a href=""><div class="text-center"><i class="icon-list-alt" id="menuicons"></i></br><span>Customer Details</span></div> </a> </li>
        <li><a href=""><div class="text-center"><i class="icon-facetime-video" id="menuicons"></i></br><span>Tracking Details</span></div> </a></li>
        <li><a href=""><div class="text-center"><i class="icon-bar-chart" id="menuicons"></i></br><span>Tracking Map</span></div> </a> </li>
        <li><a href=""><div class="text-center"><i class="icon-code" id="menuicons"></i></br><span>Set Routes</span></div> </a> </li>
        <li><a href=""><div class="text-center"><i class="icon-code" id="menuicons"></i></br><span>Admin Details</span></div> </a> </li>
        <!--li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><div class="text-center"><i class="icon-long-arrow-down" id="menuicons"></i></br><span>Drops</span></div> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="">Icons</a></li>
            <li><a href="">FAQ</a></li>
            <li><a href="">Pricing Plans</a></li>
            <li><a href="">Login</a></li>
            <li><a href="">Signup</a></li>
            <li><a href="">404</a></li>
          </ul>
        </li-->
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>




<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

 
<script src="<?php echo base_url();?>assets/js/base.js"></script> 


<!--<script>
// Create a clone of the menu, right next to original.
$('.menu').addClass('original').clone().insertAfter('.menu').addClass('cloned').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').css('margin-top','-40px').css('height','155px').css('background','rgba(41, 128, 185,0.40) ').removeClass('original').hide();

scrollIntervalID = setInterval(stickIt, 10);


function stickIt() {

  var orgElementPos = $('.original').offset();
  orgElementTop = orgElementPos.top;               

  if ($(window).scrollTop() >= (orgElementTop)) {
    // scrolled past the original position; now only show the cloned, sticky element.

    // Cloned element should always have same left position and width as original element.     
    orgElement = $('.original');
    coordsOrgElement = orgElement.offset();
    leftOrgElement = coordsOrgElement.left;  
    widthOrgElement = orgElement.css('width');
    $('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
    $('.original').css('visibility','hidden');
  } else {
    // not scrolled past the menu; only show the original menu.
    $('.cloned').hide();
    $('.original').css('visibility','visible');
  }
}

</script-->

<script>
      function initMap() {
        var mapDiv = document.getElementById('map');
        var map = new google.maps.Map(mapDiv, {
          center: {lat: 44.540, lng: -78.546},
          zoom: 8
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap"
        async defer></script>


</body>
</html>
