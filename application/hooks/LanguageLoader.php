<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');
        $ci->lang->load('message','hindi');
    }
}
