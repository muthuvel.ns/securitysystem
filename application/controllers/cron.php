<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class Cron extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
   $this->load->database();
   $this->load->model('Tracking_model','',TRUE);
   $this->load->model('User','',TRUE);
   $this->load->model('Utils','',TRUE);
   $this->load->model('Cron_model','',TRUE);
   
 }
 
 
 /**
  * cron run every 30 min
  */
 function index(){
 	
 /******************************** user tracking point calculation user going different way*************************************************/
 	$groupby = 'ut.user_guid';
 	$userTrackPoints  =   $this->User->userTrackingDetails( $groupby );
 	
 	$trackList	=	array();
 	if ( !empty( $userTrackPoints ) ) {
 		foreach ( $userTrackPoints as $usr=>$point) {
 			$userPoints  =   $this->User->userTrackingDetails( '', '', $point['user_guid'] );
 			if ( !empty( $userPoints ) ) {
 				foreach ( $userPoints as $k => $val ){ 
 					$trackList[$val['user_guid']]['tracking_id'][]	=	$val['guid'];
 					$trackList[$val['user_guid']]['device_id'][]	=	$val['device_id'];
 				}
 			}
 		}
 	}
 	
 	if (!empty( $trackList ) ) {
 		foreach ($trackList as $userGuid => $data) {
 			if ( count($data['tracking_id'])==2) {
	 			$wayPointsData1 	=   $this->Tracking_model->wayPointsForMail( $data['tracking_id'] );
		 		if ( !empty( $wayPointsData1 ) )	{
			 		if ( ( $wayPointsData1[0]['lat'] !== $wayPointsData1[1]['lat'] )  || ( $wayPointsData1[0]['lng'] !== $wayPointsData1[1]['lng'] ) ) {
		 				$userInfo = $this->User->userAndProfileDetails( $userGuid );
		 				$username	=	(!empty( $userInfo[0]['username'] )? $userInfo[0]['username']:'XXXXXX' );
		 				$phone		=	(!empty( $userInfo[0]['phone'] )? $userInfo[0]['phone']:'XXXXXX' );
		 				$message	=	"This User Going To Different places: ".$username." mobile-no: ".$phone;
		 				$admin		=	'9789191180'; //Owner Number(Admin)
		 				/** send sms */
		 				$send  =   0;//$this->Cron_model->indiaSms( $admin,$message );
		 				$to						= 'karthik@kevellcorp.com';
		 				$from					= 'karthik@kevellcorp.com';
		 				$subject				= "security system";
		 				$mail = $this->Cron_model->sendHtmlMail($to,$from,$subject,$message);
		 				if ( empty( $send ) ){
		 			//		$send  =   $this->Cron_model->sendSmsTextLocal( $admin,$message );
		 				}
		 				if ( $send ) {
		 					$filedName	=	'tracking_id';
		 					$updateData[0]	= array( 'tracking_id'=>$data['tracking_id'][0], 'mail'=>1);
		 					$updateData[1]	= array( 'tracking_id'=>$data['tracking_id'][1], 'mail'=>1);
		 					/** update mail status in tracking_points */
		 					$UpdatePoints  =   $this->Tracking_model->BulkUpadateWayPoints( $filedName, $updateData );
		 					
		 					$trackId1	=	(!empty($data['tracking_id'][0])?$data['tracking_id'][0]:'');
		 					$trackId2	=	(!empty($data['tracking_id'][1])?$data['tracking_id'][1]:'');
		 					$info1   =   $this->Usermanager->getEnableUserAndDeviceInfo( $trackId1 );
		 					$info2   =   $this->Usermanager->getEnableUserAndDeviceInfo( $trackId2 );
		 					
		 					/** notification log entry */
		 					$date	= date(DATE_TIME_FORMAT);
		 					$notification[0]=	array(	'user_guid'			=>$userGuid,
							 							'device_id'			=>(!empty($data['device_id'][0])?$data['device_id'][0]:''),
							 							'client_date'		=>$date,
							 							'activity_data1'	=>$message,
							 							'activity_data2'	=>$admin,
		 												'activity_data3'	=>json_encode($info1),
							 							'activity_comment'	=>'message send successfully',
							 							'created'			=>$date,
							 							'created_by'		=>$trackId1,
							 					);
		 					
		 					$notification[1]=	array(	'user_guid'			=>$userGuid,
							 							'device_id'			=>(!empty($data['device_id'][1])?$data['device_id'][1]:''),
							 							'client_date'		=>$date,
							 							'activity_data1'	=>$message,
							 							'activity_data2'	=>$admin,
		 												'activity_data3'	=>json_encode($info2),
							 							'activity_comment'	=>'message send successfully',
							 							'created'			=>$date,
							 							'created_by'		=>$trackId2,
							 					);
		 					
		 					$addnotification	= $this->Cron_model->insertNotificationLog( $notification );
		 				}
			 		}
		 		}
 			}
 		}
 	}
 /******************************End***********************************************************************************************************************/	
    
 /********************* start point end point calculation ( sorce and destination duration calculation ) ******************************************************************/
 	$trackPoints  =   $this->Tracking_model->trackingPointsForMail();
 	
 	if (!empty( $trackPoints ) ) {
 		
 		foreach ( $trackPoints as $points){
 			if ( strtotime($points['end_date']) <= strtotime(date('Y-m-d H:i:s')) ){
 				$userExists = $this->User->userTrackingDetails( '', '', '', $points['tracking_id']);
 				$userGuid	=	(!empty( $userExists[0]['user_guid'] ) ? $userExists[0]['user_guid'] : '');
 				$deviceId	=	(!empty( $userExists[0]['device_id'] ) ? $userExists[0]['device_id'] : '');
 				
 				if (!empty( $userGuid )) {
				 	$userInfo = $this->User->userAndProfileDetails( $userGuid );
				 	$username	=	(!empty( $userInfo[0]['username'] )? $userInfo[0]['username']:'XXXXXX' );
				 	$phone		=	(!empty( $userInfo[0]['phone'] )? $userInfo[0]['phone']:'XXXXXX' );
				 	$message = "Duration not reach by  DeviceId: $deviceId  username:".$username." mobile-no: ".$phone;
				 	$admin	='9789191180'; //Owner Number(Admin)
				 	/** send sms */
				 	$send  =   0;//$this->Cron_model->indiaSms( $admin,$message );
				 	$to						= 'karthik@kevellcorp.com';
				 	$from					= 'karthik@kevellcorp.com';
				 	$subject				= "security system";
				 	$mail = $this->Cron_model->sendHtmlMail($to,$from,$subject,$message);
				 	if ( empty( $send ) ){
				 	//	$send  =   $this->Cron_model->sendSmsTextLocal( $admin,$message );
				 	}
				 	
				 	if ( $send ) {
				 		/** update mail status in tracking_points */
				 		$update = array( 'mail'=>1);
				 		$updateTrackPoints  =   $this->Tracking_model->updateTrackingPoints( $points['tracking_id'], $update);
				 		
				 		$info1   =   $this->Usermanager->getEnableUserAndDeviceInfo( $points['tracking_id'] );
				 		
				 		/** notification log entry */
				 		$date	= date(DATE_TIME_FORMAT);
				 		$notification[0]=	array(	'user_guid'			=>$userGuid,
									 				'device_id'			=>$deviceId,
									 				'client_date'		=>$date,
									 				'activity_data1'	=>$message,
									 				'activity_data2'	=>$admin,
				 									'activity_data3'	=>json_encode($info1),
									 				'activity_comment'	=>'message send successfully',
									 				'created'			=>$date,
									 				'created_by'		=>(!empty($points['tracking_id'])?$points['tracking_id']:''),
									 		);
				 		
				 		$addnotification	= $this->Cron_model->insertNotificationLog( $notification );
				 	}
 				} 
 			}
 		}
 	}
 	
 /******************************End***********************************************************************************************************************/
 
 /** waypoint calculation (signal loss or one point to another point duration changed then send sms ) */
 	$wayPoints  =   $this->Tracking_model->wayPointsForMail();
 	
 	if (!empty( $wayPoints ) ) {
 			
 		foreach ( $wayPoints as $way){
 			if ( strtotime($way['end_date']) <= strtotime(date('Y-m-d H:i:s')) ){
 					
 				$userExists = $this->User->userTrackingDetails( '', '', '', $way['tracking_id']);
 				$deviceId	=	(!empty( $userExists[0]['device_id'] ) ? $userExists[0]['device_id'] : '');
 				$userGuid	=	(!empty( $userExists[0]['user_guid'] ) ? $userExists[0]['user_guid'] : '');
 				if (!empty( $userGuid )) {
	 				
	 				$userInfo = $this->User->userAndProfileDetails( $userGuid );
	 				$username	=	(!empty( $userInfo[0]['username'] )? $userInfo[0]['username']:'XXXXXX' );
	 				$phone		=	(!empty( $userInfo[0]['phone'] )? $userInfo[0]['phone']:'XXXXXX' );
	 				
	 				$message ="signal Loss by DeviceId: $deviceId username:".$username."mobile-no:".$phone;
	 				$admin	='9789191180';
	 				/** send sms */
	 				$send  =   0;//$this->Cron_model->indiaSms( $admin,$message );
	 				$to						= 'karthik@kevellcorp.com';
	 				$from					= 'karthik@kevellcorp.com';
	 				$subject				= "security system";
	 				$mail = $this->Cron_model->sendHtmlMail($to,$from,$subject,$message);
	 				
	 				if ( empty( $send ) ){
	 				//	$send  =   $this->Cron_model->sendSmsTextLocal( $admin,$message );
	 				}
	 				if ( $send ) {
		 				/** update mail status in way_points */
		 				$update = array( 'mail'=>1);
		 				$wayPoints  =   $this->Tracking_model->updateWayPoints( $way['tracking_id'], $update);
		 				
		 				$info1   =   $this->Usermanager->getEnableUserAndDeviceInfo( $way['tracking_id'] );
		 				
		 				/** notification log entry */
				 		$date	= date(DATE_TIME_FORMAT);
				 		$notification[0]=	array(	'user_guid'			=>$userGuid,
									 				'device_id'			=>$deviceId,
									 				'client_date'		=>$date,
									 				'activity_data1'	=>$message,
									 				'activity_data2'	=>$admin,
				 									'activity_data3'	=>json_encode($info1),
									 				'activity_comment'	=>'message send successfully',
									 				'created'			=>$date,
									 				'created_by'		=>(!empty($way['tracking_id'])?$way['tracking_id']:''),
									 		);
				 		
				 		$addnotification	= $this->Cron_model->insertNotificationLog( $notification );
	 				}
 				}
 			}
 	
 		}
 	}
 	exit;
 	/******************************End***********************************************************************************************************************/
 	  
 }
 
 
 
 function test()
 {
 	/*
 		$deviceId	=	'91d0dfeff6c9de28f8f3959fb67b7816';
 		$userGuid	=	'e4c69b1287cb8e98e0f08ae5b34e6277';
 		$message	=	"signal Loss by DeviceId: $deviceId username: Muthu mobile-no:9365645644";
 		$admin		=	'9025940063';
 		$way['tracking_id']	=	'7a596d8609663d736c9a1c17b4464337';
 		
 		$date	= date(DATE_TIME_FORMAT);
 		$notification[0] =	array(	'user_guid'			=>$userGuid,
					 				'device_id'			=>$deviceId,
					 				'client_date'		=>$date,
					 				'activity_data1'	=>$message,
					 				'activity_data2'	=>$admin,
					 				'activity_comment'	=>'message send successfully',
					 				'created'			=>$date,
					 				'created_by'		=>(!empty($way['tracking_id'])?$way['tracking_id']:''),
					 		);
				 		
 		$addnotification	= $this->Cron_model->insertNotificationLog( $notification );
 		echo '<pre>';print_r($addnotification);exit;*/
 }
 
  function sendmail(){
 	
	$to = "likememv@gmail.com";
	$subject = "hai success";
	/*
	$message ="HAi";
	
	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "From: muthuvel.ns@gmail.com" . "\r\n" .	"Reply-To: likememv@gmail.com" . "\r\n" .	"X-Mailer: PHP/" . phpversion();
	
	$result = mail($to,$subject,$message,$headers);*/
	
	$filename = 'sample.pdf';
	$path  =$_SERVER['DOCUMENT_ROOT'].'/assets/upload_images';
	$file = $path . "/" . $filename;
	
	$file_size = filesize($file);
	$handle = fopen($file, "r");
	$content = fread($handle, $file_size);
	fclose($handle);
	$content = chunk_split(base64_encode($content));
	
	// a random hash will be necessary to send mixed content
	$separator = md5(time());
	
	// carriage return type (we use a PHP end of line constant)
	$eol = PHP_EOL;
	
	// main header (multipart mandatory)
	$headers = "From: name <test@test.com>" . $eol;
	$headers .= "MIME-Version: 1.0" . $eol;
	$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
	$headers .= "Content-Transfer-Encoding: 7bit" . $eol;
	$headers .= "This is a MIME encoded message." . $eol;
	
	// message
	$headers .= "--" . $separator . $eol;
	$headers .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . $eol;
	$headers .= "Content-Transfer-Encoding: 8bit" . $eol;
	$headers .= $message . $eol;
	
	// attachment
	$headers .= "--" . $separator . $eol;
	$headers .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
	$headers .= "Content-Transfer-Encoding: base64" . $eol;
	$headers .= "Content-Disposition: attachment" . $eol;
	$headers .= $content . $eol;
	$headers .= "--" . $separator . "--";
echo '<pre>';print_r($headers);	//SEND Mail
	if (mail($to, $subject, "", $headers)) {
		echo "mail send ... OK"; // or use booleans here
	} else {
		echo "mail send ... ERROR!";
	}
	
	echo '<pre>';print_r('success');exit;
 	
  } 
  

  function mail(){
  	require_once('libraries/class.phpmailer.php');
  	 
  	$mail = new PHPMailer(); // create a new object
//   	echo '<pre>';print_r($mail);exit;
  	$mail->IsSMTP(); // enable SMTP
  	$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
  	$mail->SMTPAuth = true; // authentication enabled
  	$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
  	$mail->Host = "smtp.gmail.com";
  	$mail->Port = 465; // or 587
  	$mail->IsHTML(true);
  	$mail->Username = "deepaklinux07@gmail.com";
  	$mail->Password = "itechnion123";
  	$mail->SetFrom("amdeepak21@gmail.com");
  	$mail->Subject = "Test";
  	$mail->Body = "hello";
  	$mail->AddAddress("amdeepak21@gmail.com");
  	echo '<pre>';print_r($mail);exit;
  	if(!$mail->Send()) {
  		echo "Mailer Error: " . $mail->ErrorInfo;
  	} else {
  		echo "Message has been sent";
  	}
  	
  
  	//Send user activation mail notification
  	$recipient_emails		= 'likememv@gmail.com';
  	$subject				= "security system";
  	$message 				= 'Hi Admin, <br /><br /> Route has been changed. Below is the info <br /><br /> ';
  	$this->Cron_model->sendHtmlMail($recipient_emails,'muthuvel.ns@gmail.com',$subject,$message);  
  }
}
 
?>
