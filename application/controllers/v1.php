<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class V1 extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
	 // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

  
   $this->load->database();
   $this->load->helper(array('url','language'));
   $this->load->model('Tracking_model','',TRUE);
   $this->load->model('User','',TRUE);
   $this->load->model('Usermanager','',TRUE);
   $this->load->model('Trackingmanager','',TRUE);
   $this->load->model('Login_model','',TRUE);
   $this->load->model('Utils','',TRUE);
 }
 
 function login(){
 	$requestData = json_decode(file_get_contents('php://input'), true);
 	
 	if(!empty( $requestData ) ) {
 		
 		$username	= (!empty( $requestData['username'] )?strtolower($requestData['username']):'');
 		$password	= (!empty( $requestData['password'] )?$requestData['password']:'');
 		
 		if ( !empty( $username ) || !empty( $password )) {
	 		$reqdata	=	array( 'username'=> $username,'password'=>$password );
	 		$admindata	=	$this->Login_model->loginValidation( $reqdata );
	 		if ( empty( $admindata )) {
	 			$userdata	=	$this->Login_model->loginValidationSupplier( $reqdata );
	 			$role	 	= 'supllier';
	 		}else{
	 			$userdata	= $admindata;
	 			$role		= $admindata[0]['roleName'];
	 		}
	 		
	 		if ( !empty( $userdata ) )	{
	 			/** check email count is one then redirect to crossponding user domain*/
	 			if( count($userdata) == 1 ) {/** User has one role */
	 				
	 				$resultData		=	array();
					$appData		=	array();
					$appDeviceId	=	'';
					
					if( isset( $requestData['app_device_id'] ) && !empty( $requestData['app_device_id'] ) ){
						$appDeviceId	=	trim( $requestData['app_device_id'] );
					}
					
					$appType 	= (!empty($requestData['app_type'])?$requestData['app_type']:'');
					$deviceType = (!empty($requestData['device_type'])?$requestData['device_type']:'');
					
					/** Insert App details */
					$checkAppData = $this->Login_model->getSecurityAppDetails($userdata['0']['userGuid'], $appType, $appDeviceId);
					
					if( empty( $checkAppData ) ) {
						$installCount	=	1;
					} else {
						$countApp		=	count( $checkAppData );
						$installCount	=	$countApp + 1;
					}
					
					$currentDate		=	date( DATE_TIME_FORMAT);
					$guid			=	$this->Utils->getGuid(  );
					
					$appData			=	array(
												'app_device_id'		=> $appDeviceId,
												'user_guid'			=> $userdata['0']['userGuid'],
												'device_type'		=> $deviceType,
												'app_type'			=> $appType,
												'login_date'		=> $currentDate,
												'install_count'		=> $installCount,
												'created'			=> $currentDate,
												'created_by'		=> $userdata['0']['userGuid'],
												'last_updated'		=> $currentDate,
												'last_updated_by'	=> $userdata['0']['userGuid'],
												'guid'				=> $guid
											);
					
					$appActivity	= $this->Login_model->insertSecurityApp( $appData );
	 				
	 				
	 				/** Login activity  */
	 				$loginActivity	= $this->Login_model->loginActivityEntry( $userdata['0']['userGuid'], 2, 'Mobile APP' );
	 				
	 				$result['statusCode']   =  200;
	 				$result['status']   	=  'Success';
	 				$result['message']  	=  'Login successfully';
	 				$result['data']  		=  $role;
	 			} else {
	 				$result['statusCode']   =  404;
	 				$result['status']   	=  'Failure';
	 				$result['message']  	=  'Username or Password you entered is incorrect';
	 			}
	 		}else{
	 			$result['statusCode']   =  404;
	 			$result['status']   	=  'Failure';
	 			$result['message']  	=  'Username or Password you entered is incorrect';
	 		}
 		}else{
 			$result['statusCode']   =  404;
 			$result['status']   	=  'Failure';
 			$result['message']  	=  'Username or Password Shoud not Empty';
 		}
 	}else{
 		$result['statusCode']   =  404;
 		$result['status']   	=  'Failure';
 		$result['message']  	=  'Request Data Empty';
 	}
 	
 	header('Content-type: application/json');
 	echo  json_encode($result);
 }
 
 
 /**
  * Push waypoints(marker points)
  */
 function pushwaypoints(){
 	
 	$requestData = json_decode(file_get_contents('php://input'), true); 
 	$deviceId	=   (!empty($requestData['device_id'])?$requestData['device_id']:'');
 	$lat 		=   (!empty($requestData['lat'])?$requestData['lat']:'');
 	$lng	 	=   (!empty($requestData['lng'])?$requestData['lng']:'');
 	
 	if( empty( $deviceId ) ){
 		$result['message']		=  "Shoud not empty DeviceId";
 		$result['status'] 		=   'Failure';
 		$result['statusCode']	=   '404';
 	}elseif (empty( $lat )) {
 		$result['message']		=  "Shoud not empty  Lat ";
 		$result['status'] 		=   'Failure';
 		$result['statusCode']	=   '404';
	}elseif (empty( $lng )) {
		$result['message']		=  "Shoud not empty Lng ";
		$result['status'] 		=   'Failure';
		$result['statusCode']	=   '404';
	}else{
 		$deviceExists	=	$this->User->userTrackingDetails( '', $deviceId );
 		
 		if ( !empty( $deviceExists[0]['guid'] ) ) {
 			
 			$trackingGuid	=	$deviceExists[0]['guid'];
 			$update	=	array( 'status' => 0);
 			$updateWayPoints	=	$this->Tracking_model->updateWayPoints($trackingGuid, $update);
 			
			$time    		= WAYPOIND_TIMING;
			$startDate 		= date(DATE_TIME_FORMAT);
			$newtimestamp 	= strtotime("$startDate + $time" );
			$endDate		= date(DATE_TIME_FORMAT, $newtimestamp);
			
			$way_points[] = array(
					                 'tracking_id'	=>$trackingGuid,
					                 'lat'    		=>$lat,
					                 'lng'    		=>$lng,
					                 'start_date'   =>$startDate,
					                 'end_date'     =>$endDate,
					                 'status'     	=>1
                                   );
 			$inserWayPoints		=	$this->Tracking_model->insertWayPoints( $way_points );
 			
 			if ( $inserWayPoints ) {
 				$result['statusCode']   =  '200';
 				$result['message'] =  "Inserted successfully";
 				$result['status'] =   'Success';
 			} else {
 				$result['statusCode']   =  '404';
 				$result['message'] =  "Inserted Failure please try again";
 				$result['status'] =   'Failure';
 			}
 		} else {
 			$result['statusCode']   =  '404';
 			$result['message'] =  "DeviceID not exists please initiate route";
 			$result['status'] =   'Failure';
 		}
 	}
 	
 	 header('Content-type: application/json');   
        echo  json_encode($result); 
 }
 
 /**
  * To set source and destination route for user and device
  */
 function pushroute(){
 	$requestData = json_decode(file_get_contents('php://input'), true);
 	
 	 $flag =   0;
	 $message='';
	if (empty($requestData['device_id'])  ) {
		$message	=	'Argument Missing DeviceID';
	}elseif ( empty($requestData['username'])) {
		$message	=	'Argument Missing username';
	}elseif ( empty($requestData['start_lat'])) {
		$message	=	'Argument Missing start_lat';
	}elseif ( empty($requestData['start_lng'])) {
		$message	=	'Argument Missing start_lng';
	}elseif ( empty($requestData['end_lat'])) {
		$message	=	'Argument Missing end_lat';
	}elseif ( empty($requestData['end_lng'])){ 
		$message	=	'Argument Missing end_lng';
	}elseif ( empty($requestData['hour'])){
		$message	=	'Argument Missing hour';
	}elseif ( empty($requestData['min'])){
		$message	=	'Argument Missing min';
 	}else {
 		
 		$userExists	=	$this->User->userDetails( $requestData['username'] );
 		if ( !empty( $userExists[0]['guid'] )) {
 			$userGuid		=   $userExists[0]['guid'];
 			$userProfile	=	$this->User->userProfileDetails( $userGuid );
 		}
 		
 		$deviceId		=   $requestData['device_id'];
 		$start_lat		=   $requestData['start_lat'];
 		$start_lng		=   $requestData['start_lng'];
 		$end_lat		=   $requestData['end_lat'];
 		$end_lng		=   $requestData['end_lng'];
 		$hour			=   $requestData['hour'];
 		$min	     	=   $requestData['min'];
 		
 		$insertUserTracking	= 0;
 		$insertWayPoint		= 0;
 		$insertTrackPoints	= 0;
 		/** DB transaction */
 		$this->db->trans_begin();
 		
 		if ( !empty( $userProfile[0]['device_id'] )){
          	$userExists = $this->User->userTrackingDetails( '', $deviceId, $userGuid );
 	      	$deviceData = array();
 	    	$created	=	date(DATE_TIME_FORMAT);
 	      if (empty( $userExists ) ) {
 	      	   $deviceGuid  =   $this->Utils->getGuid();
 	      	   $deviceData[]   =   array( 'device_id'     	=>  $deviceId,
							 	      	  'user_guid'    	=>  $userGuid,
							 	      	  'created'      	=>  $created,
										  'created_by'      =>  'unknown',
					               		  'last_updated'   	=>  $created,
					               		  'last_updated_by' =>  'unknown',
							 	      	  'guid'         	=>  $deviceGuid
 	      	                       );
 	      	 
 	      }
 	      
 	      if(!empty( $deviceData )){
		 	      $userdeviceId	=  $userProfile[0]['device_id'] ;
		 	      $userExists1	= $this->User->userTrackingDetails( '', $userdeviceId, $userGuid );
		 	      $userDeviceData = array();
		 	      if (empty( $userExists1 ) ) {
		               $userDeviceGuid  =   $this->Utils->getGuid();
		               $userDeviceData[]   =   array(     'device_id'    =>  $userdeviceId,
					                                      'user_guid'    =>  $userGuid,
					                                      'created'      	=>  $created,
														  'created_by'      =>  'unknown',
									               		  'last_updated'   	=>  $created,
									               		  'last_updated_by' =>  'unknown',
					                                      'guid'         =>  $userDeviceGuid
				                                   );
		          }
		          
		          if ( !empty( $deviceData ) && !empty( $userDeviceData )) {
		 	          $data   =   array_merge($deviceData,$userDeviceData );
		 	          $insertUserTracking  =   $this->User->insertUserTrackingInfo($data);
		          }
		          
		 	    if ( !empty( $insertUserTracking ) ) {
		 	        /** insert tracking points */
		            $startDate 		= date(DATE_TIME_FORMAT);
			 	    $time			= $hour.' hours '.$min.' mins';
			 	    $newtimestamp 	= strtotime("$startDate + $time" );
			 	    $endDate		= date(DATE_TIME_FORMAT, $newtimestamp);
			 	    
		            $track_points[0] = array(
		                                'tracking_id'  =>$deviceGuid,
		                                'start_lat'    =>$start_lat,
							            'start_lng'    =>$start_lng,
							            'end_lat'      =>$end_lat,
							            'end_lng'      =>$end_lng,
		                                 'start_date'   =>$startDate,
		                                 'end_date'     =>$endDate
		                                 );
		          $track_points[1] = array(
                                        'tracking_id'  =>$userDeviceGuid,
                                        'start_lat'    =>$start_lat,
                                        'start_lng'    =>$start_lng,
                                        'end_lat'      =>$end_lat,
                                        'end_lng'      =>$end_lng,
                                         'start_date'   =>$startDate,
                                         'end_date'     =>$endDate
                                         );
		          
		            $insertTrackPoints = $this->Tracking_model->insertTrackingPoints( $track_points );
		           
		            if ( $insertTrackPoints ){
		            	$time    =   WAYPOIND_TIMING;
		            	$newtimestamp = strtotime("$startDate + $time" );
                        $endDate= date(DATE_TIME_FORMAT, $newtimestamp);
                    
		            	$way_points[0] = array(
                                        'tracking_id'  =>$deviceGuid,
                                        'lat'    =>$start_lat,
                                        'lng'    =>$start_lng,
                                         'start_date'   =>$startDate,
                                         'end_date'     =>$endDate,
		            	                 'status'     =>1
                                         );
                         $way_points[1] = array(
                                        'tracking_id'  =>$userDeviceGuid,
                                        'lat'    =>$start_lat,
                                        'lng'    =>$start_lng,
                                         'start_date'   =>$startDate,
                                         'end_date'     =>$endDate,
                                        'status'     =>1
                                         );
                                         
		            	$insertWayPoint = $this->Tracking_model->insertWayPoints( $way_points );
		            }else{
		            	$message	=	"Tracking point does't not insert";
		            }
		 	     }else{
		 	     	$message	=	"User Tracking point does't not insert";
		 	     }
 	      }else{
 	      	$message	=	'deviceID and UserID already Used';
 	      }
 		}else{
 			$message	=	'This user does not have DeviceID';
 		}
 		if ($this->db->trans_status() === FALSE)
 		{
 			$this->db->trans_rollback();
 		}
 		else
 		{
 			if (empty($insertUserTracking) || empty( $insertTrackPoints ) || empty( $insertWayPoint )){
 				$this->db->trans_rollback();
 			}else {
 				$this->db->trans_commit();
 				$flag =   1;
 				$message	=	'Successfully inserted';
 			}
 		}
 	}
 	
 	$result['statusCode']  	=  404;
 	$result['status']  		=  'Failure';
 	$result['message'] 		=  $message;
 	
 	if ($flag==1){
 		$result['statusCode']  	=  200;
 		$result['status'] 		=  'Success';
 	 	$result['message'] 		=  $message;
 	}
 	
 	header('Content-type: application/json');
 	echo  json_encode($result);
 	
 }

 /**
  * generate device information
  */
 function pushboxdeviceinfo(){
 	$requestData = json_decode(file_get_contents('php://input'), true);
 	
 	if (empty( $requestData['device_id'])) {
 		$result['statusCode']   =  '404';
 		$result['message'] =  'Request data empty device_id';
 		$result['status']  =  'Failure';
 		
 	}elseif (empty( $requestData['device_name'])) {
 		$result['statusCode']   =  '404';
 		$result['message'] =  'Request data empty device_name';
 		$result['status']  =  'Failure';
 		
 	}elseif (empty( $requestData['loginusername'] ) ) {
 		$result['statusCode']   =  '404';
 		$result['message'] =  'Request data empty loginusername';
 		$result['status']  =  'Failure';
 		
 	}else{
	    $deviceGuid  =   $requestData['device_id'];
	    
	    $deviceExists=	$this->Tracking_model->deviceInfo( $deviceGuid );
	    if ( empty( $deviceExists ) ) {
	    	$loginuseremail = $requestData['loginusername'];
	    	/** login user name validation */
	    	$adminUserExists	=	$this->User->userDetails( $loginuseremail );
	    	
	    	if ( !empty( $adminUserExists[0]['guid']) ) {
			    $pwd         =   $this->Utils->getRandomString();
			   
			    if ( !empty( $requestData['password'] ) ) {
			    	$pwd	=	$requestData['password'];
			    }
		    
			    /** need to device name in request data*/
			    $created		=	date(DATE_TIME_FORMAT);
			    $deviceName		=	$requestData['device_name'];
			    $loginUserGuid	=	$adminUserExists[0]['guid'];
			    $insertDevice 	= array(
							    		'name' 				=>	$deviceName,
			    						'device_id'			=>	$deviceGuid,
			    						'password'			=>	$pwd,
							    		'created'			=>  $created,
							    		'created_by'		=>  $loginUserGuid,
							    		'last_updated'      =>  $created,
							    		'last_updated_by'   =>  $loginUserGuid
							    );
			    
			    $insert = $this->Tracking_model->insertTrackingInfo( $insertDevice );
			    
			    if ( $insert ) {
			    	$result['statusCode']   =  '200';
				    $result['message'] =  "Inserted successfully";
				    $result['status']  =  'Success';
			    }else {
			    	$result['statusCode']   =  '404';
			    	$result['message'] =  "Failure to insert Please try again after some time";
			    	$result['status']  =  'Failure';
			    }
		    }else{
		    	$result['statusCode']   =  '404';
		    	$result['message'] =  "Invalid loginusername: ".$requestData['loginusername'] ;
		    	$result['status']  =  'Failure';
	    	}
	    }else{
	    	$result['statusCode']   =  '404';
	    	$result['message'] =  "DeviceId already exists";
	    	$result['status']  =  'Failure';
	    }
 	}
 	
    header('Content-type: application/json');
 	echo  json_encode($result);
      
 }
 
 /**
  * push user device info (mobile device_id)
  */
 function pushuserdeviceinfo(){
 	$requestData = json_decode(file_get_contents('php://input'), true);
 	$email		=	(isset( $requestData['username'])? $requestData['username'] :'');
 	$deviceId	=	(isset( $requestData['device_id'])? $requestData['device_id'] :'');

 	if ( empty( $deviceId )) {
 		$result['statusCode']   =  '404';
 		$result['message']  =  'Request data empty device_id';
 		$result['status']   =  'Failure';
 	}elseif(empty( $email )){
 		$result['statusCode']   =  '404';
 		$result['message']	=  'Request data empty email';
 		$result['status']   =  'Failure';
 	}elseif ( !empty( $deviceId ) && !empty( $email )) {
 		$userExists	=	$this->User->userDetails( $email );
 		if ( !empty( $userExists[0]['guid'] )) {
 			$userProfileExists	=	$this->User->userProfileDetails( $userExists[0]['guid'] );
 		}
 		
 		if ( !empty( $userProfileExists  ) ) {
	 		$updateData	=	array('device_id'=> $deviceId, 'last_updated'=>date(DATE_TIME_FORMAT), 'last_updated_by'=>$userExists[0]['guid']);
	 		$this->User->updateUserProfile($userExists[0]['guid'], $updateData);
	 		
	 		$result['statusCode']   =  '200';
	 		$result['message']  =  'Successfully added';
	 		$result['status']   =  'success';
	 		
 		} else {
 			$result['statusCode']   =  '404';
	 		$result['message']  =  'profile info does not exists this user';
	 		$result['status']   =  'Failure';
 		}
 	}
 	header('Content-type: application/json');
 	echo json_encode($result);
 }
 
 /**
  * get over all device information
  * @return json
  */
 function getdevicelocationinfo()
 {
//  	$requestData = json_decode(file_get_contents('php://input'), true);
 	$trakingIdList	= array();
 	$data			= array();
 	$userTracking  =   $this->User->userTrackingDetails();
 	if (!empty( $userTracking ) ) {
 		foreach ( $userTracking as $user) { 
 			/** validate deviceID is exits or not. if exits add the list otherwise omited */
 			$userProfile  =   $this->User->userProfileDetails('', $user['device_id']);
 			$trackingInfo =   $this->Tracking_model->deviceInfo($user['device_id']);
 			$flag = ( !empty( $userProfile ) ? 1: 0);
 			$flag = ( !empty( $trackingInfo ) ? 1: 0);
			/** if flag == 1 then add to trackingId*/
 			if ( $flag ==1){ $trakingId[$user['user_guid']][] = $user['guid'];}
 		}
 		
 		/** get user based count list .User have two trackingId then it'll show for map otherwise remove the tracking */
 		$list	=	array();
 		if ( !empty( $trakingId ) ) {
 			foreach ( $trakingId as $id) {
 				if ( count($id) == 2){
 					$list[] = $id;
 				}
 			}
			/** two dimentional to single dimention array conversion*/
 			$trakingIdList = call_user_func_array('array_merge', $list);
 		}
 	}
 	
 	if ( !empty( $trakingIdList )) {
 		$data	=	$this->Trackingmanager->getDeviceDetails( $trakingIdList );
 	}
 
 	if ( empty( $data[0] ) ) {
 		$result['statusCode']   =  '404';
 		$result['message']	=  'Device not found please intilize route';
 		$result['status']	=   'Failure';
 	}else{
 		$result['statusCode']   =  '200';
 		$result['message']  =  'Device information';
 		$result['status']   =  'Success';
 		$result['data']  	=   $data;
 	}
 	// echo '<pre>';print_r(json_encode($result ));exit;
 	header('Content-type: application/json');
 	echo  json_encode($result);
 }
 
 
 /**
  * get device information based on given device_id
  */
 function getindividualdevicelocationinfo() {
 	$requestData 		= json_decode(file_get_contents('php://input'), true);
 	$deviceId			=	(!empty( $requestData['device_id'] ) ? $requestData['device_id'] : '');
 	$username			=	(!empty( $requestData['username'] ) ? $requestData['username'] : '');

 	if ( empty( $deviceId ) && empty( $username) ){
 		$result['message']  =  'Request data shoud not empty';
 		$result['status']   =  'Failure';
 		$result['statusCode']   =  '404';
 	}else{
 		$userGuid = '';
 		if ( !empty( $username )) {
	 		$userExists	=	$this->User->userDetails( $username );
	 		$userGuid = ( !empty( $userExists[0]['guid'] ) ? $userExists[0]['guid'] : '') ;
 		}
 		
 		$data	=	$this->Trackingmanager->getDeviceDetails('', $deviceId, $userGuid );
 		 
 		if ( empty($data[0]) ) {
 			$result['message']	=  'Details not found please intilize route';
 			$result['status']	=   'Failure';
 			$result['statusCode']	=   '404';
 		}else{
 			$result['statusCode']	=   '200';
 			$result['message']  =  'Device information';
 			$result['status']   =  'Success';
 			$result['data']  	=   $data;
 		}
 	}
 	header('Content-type: application/json');
 	echo  json_encode($result);
 }
 
 /**
  * get device current location by given device_id
  * @return Json
  * {
    "statusCode": "200",
    "message": "Device information",
    "status": "Success",
    "data": { "marker": {"7a596d8609663d736c9a1c17b4464337": [ {
										                    "lat": "10.103199",
										                    "lng": "77.946947",
										                    "image": "http://tracking.com/assets/ico/car1.png",//image
										                    "username": "newuser",
										                    "mobile": "9025154687"
								                		}
           	 										]
        					}
    			}
		}
  */
 function getdevicecurrentlocation(){
 	
 	$requestData 		= json_decode(file_get_contents('php://input'), true);
 	$deviceId			=	(!empty( $requestData['device_id'] ) ? $requestData['device_id'] : '');
 	$username			=	(!empty( $requestData['username'] ) ? $requestData['username'] : '');

 	if ( empty( $deviceId ) && empty( $username) ){
 		$result['message']  =  'Request data shoud not empty';
 		$result['status']   =  'Failure';
 		$result['statusCode']   =  '404';
 	}else{
 		$userGuid = '';
 		if ( !empty( $username )) {
 			$userExists	=	$this->User->userDetails( $username );
 			$userGuid = ( !empty( $userExists[0]['guid'] ) ? $userExists[0]['guid'] : '') ;
 		}
 		
 		$data	=	$this->Trackingmanager->getUserCurrentLocation( $deviceId, $userGuid );

 		if ( empty($data['activeuser']) ) {
 			$result['statusCode']   =  '404';
 			$result['message']	=  'Location not found ';
 			$result['status']	=  'Failure';
 		}else{
 			$result['statusCode']   =  '200';
 			$result['message']  =  'Device information';
 			$result['status']   =  'Success';
 			$result['data']  	=   $data;
 		}
 	}
 	
 	header('Content-type: application/json');
 	echo  json_encode($result);
 }
 
function userregistration(){
	$requestData 		= json_decode(file_get_contents('php://input'), true);
	if(empty($requestData['username'])){
		$result['message']  	=  'username shoud not empty';
		$result['status']   	=  'Failure';
		$result['statusCode']   =  '404';
	}elseif(empty($requestData['password'])){
		$result['message']  	=  'password shoud not empty';
		$result['status']   	=  'Failure';
		$result['statusCode']   =  '404';
	}elseif(empty($requestData['name'])){
		$result['message']  	=  'name shoud not empty';
		$result['status']   	=  'Failure';
		$result['statusCode']   =  '404';
	}elseif(empty($requestData['phone'])){
		$result['message']  	=  'phone shoud not empty';
		$result['status']   	=  'Failure';
		$result['statusCode']   =  '404';
	}elseif(empty($requestData['loginusername'])){
		$result['message']  	=  'loginusername shoud not empty';
		$result['status']   	=  'Failure';
		$result['statusCode']   =  '404';
	}elseif(!$this->isValidLoginForm('username', $requestData['username'])){
		$result['message']  	=  'Invaild email id';
		$result['status']   	=  'Failure';
		$result['statusCode']   =  '404';
		$result['data']   		=  $requestData['username'];
	}elseif(!$this->isValidLoginForm('phone',$requestData['phone'])){
		$result['message']  	=  'Invalid phone number';
		$result['status']   	=  'Failure';
		$result['statusCode']   =  '404';
	}else{
		/** assign request data */
 		$email	= $requestData['username'];
 		$pass	= $requestData['password'];
 		$uname	= $requestData['name'];
 		$phone	= $requestData['phone'];
 		$loginuseremail = $requestData['loginusername'];
 		/** login user name validation */
 		$adminUserExists	=	$this->User->userDetails( $loginuseremail );
 		if ( !empty( $adminUserExists )) {
 			$sessionUserGuid	=	$adminUserExists[0]['guid'];
 				/** validate email aleady exists or not  */
		 		$userExists			=	$this->User->userDetails( $email );
		 		if ( empty( $userExists )) {
			 		$this->db->trans_begin();
			 			$userGuid  		=   $this->Utils->getGuid();
			           	$created		=	date(DATE_TIME_FORMAT);
			            $insertUserData =   array(	'email'				=> $email,
										            'password'			=> md5($pass),
										            'username'			=> $uname,
								            		'created'      		=>  $created,
								            		'created_by'      	=>  $sessionUserGuid,
							               		  	'last_updated'     	=>  $created,
							               		  	'last_updated_by'   =>  $sessionUserGuid,
		                                        	'guid'				=>	$userGuid,
									            );
			            $insertUser	    =   $this->User->insertUser($insertUserData);					   	            
			
			            if ( $insertUser ) {
				            $profileGuid  =   $this->Utils->getGuid();
				            $insertProfileData =   array(  
				                                            'user_guid' 		=> $userGuid,
				                                            'phone' 			=> $phone,
										            		'created'      		=>  $created,
										            		'created_by'      	=>  $sessionUserGuid,
									               		  	'last_updated'     	=>  $created,
									               		  	'last_updated_by'   =>  $sessionUserGuid,
												            'guid'				=>	$profileGuid,
				                                        );
				                                        
				           $insertProfile        =   $this->User->insertUserProfile($insertProfileData);   
			            }
			
			        if (empty( $insertUser ) || empty( $insertProfile ) )   {
			            $this->db->trans_rollback();
			            $result['message']  	=  'Registration Failed please try after some time';
			            $result['status']   	=  'Failure';
			            $result['statusCode']   =  '404';
			        }
			        else
			        {
			            $this->db->trans_commit();
			            $result['message']  	=  'Registration Success';
			            $result['status']   	=  'Success';
			            $result['statusCode']   =  '200';
			        }
		 		}else{
		 			$result['message']  	=  'User Already exists with same email please use another email id';
		 			$result['status']   	=  'Failure';
		 			$result['statusCode']   =  '404';
		 		}
	   	}else{
		   	$result['message']  	=  'Invalid loginuser name';
		   	$result['status']   	=  'Failure';
		   	$result['statusCode']   =  '404';
	   }
	}
	header('Content-type: application/json');
	echo  json_encode($result);
 }
 
 function isValidLoginForm( $key,  $value) {
	 switch ( $key ){
 		case 'username':{
 			/** Check email structure validation  */
 			return preg_match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$^', $value);
 			break;
 		}
 		case 'phone':{
 			/** Check mobile structure validation  */
 			return preg_match('/^[1-9][0-9]*$/', $value);
 			break;
 		}
 		default:{
 			return false;
 		}
 	}
 	
 }
 
 function locationhistroy(){
 	/** get enable user device info */
 	$output   =   $this->Usermanager->getEnableUserAndDeviceInfo();
 	
 	$result['message']  	=  'Location empty';
 	$result['status']   	=  'Failure';
 	$result['statusCode']   =  '404';
 	
 	/** validate input */
 	if (!empty($output['user']) ){
 		$result['message']  	=  'Location information';
 		$result['status']   	=  'Success';
 		$result['statusCode']   =  '200';
 		
 		foreach ($output['user'] as $key=>$value ){
 			$result['data']['locations'][] = $value;
 		}
 	}
 	header('Content-type: application/json');
 	echo  json_encode($result);
 }
 
 function getboxdevicelist(){
 	$output   =   $this->Tracking_model->deviceInfo(  );
 
 	$result['message']  	=  'Location empty';
 	$result['status']   	=  'Failure';
 	$result['statusCode']   =  '404';
 
 	if (!empty($output) ){
 		$result['message']  	=  'Location information';
 		$result['status']   	=  'Success';
 		$result['statusCode']   =  '200';
 		foreach ( $output as $key=>$value) {
 			$result['data']['deviceList'][$key]['device_id']	= $value['device_id'];
 			$result['data']['deviceList'][$key]['name']			= $value['name'];
 		}
 	}
 	header('Content-type: application/json');
 	echo  json_encode($result);
 }
 
 function getuserlist(){
 	
 	/** get user role info */
 	$userRole    =  $this->User->getUserRoleInfo();
 	
 	$inputRole['user_guid']	=	array();
 	if (!empty( $userRole)) {
 		foreach ( $userRole as $role){
 			$inputRole['user_guid'][] =   $role['userGuid'];
 		}
 	}
 	
 	$resultData		=	array();
 	$userGuidList	=	(!empty( $inputRole['user_guid'] ) ?array_unique($inputRole['user_guid']):' ');
 	if ( !empty( $userGuidList )){
 		$output   =   $this->User->userAndProfileDetails('', '', $userGuidList  );
 		
 		if (!empty( $output )) {
 			foreach ( $output as $key=>$val ) {
 				$replacements = array('photo' => base_url().'assets/upload_images/default.jpeg');
 				if ( !empty($val['photo'] )) {
 					$replacements = array('photo' => base_url().'assets/upload_images/'.$val['photo']);
 				}
 				$resultData[] = array_replace($val, $replacements);
 			}
 		}
 	}
 	$result['message']  	=  'User empty';
 	$result['status']   	=  'Failure';
 	$result['statusCode']   =  '404';
 
 	if (!empty($resultData) ){
 		$result['message']  	=  'User information';
 		$result['status']   	=  'Success';
 		$result['statusCode']   =  '200';
 		$result['data']['userList']	= $resultData;
 	}
 	header('Content-type: application/json');
 	echo  json_encode($result);
 }
 
 function getassigneduserlistformap(){
 	$groupby 	= 'ut.user_guid';
 	$userTrack  =  $this->User->userTrackingDetails($groupby);
 
 	$inputRole	=	array();
 	if (!empty( $userTrack)) {
 		foreach ( $userTrack as $role){
 			$inputRole[] =   $role['user_guid'];
 		}
 	}
 	$output	= array();
 	$userGuidList	=	(!empty( $inputRole ) ?array_unique($inputRole):' ');
 	if ( !empty( $userGuidList )){
 		$output   =   $this->User->userAndProfileDetails( $userGuidList );
 	}
 
 	$result['message']  	=  'User empty';
 	$result['status']   	=  'Success';
 	$result['statusCode']   =  '200';
 
 	if (!empty($output) ){
 		$result['message']  	=  'mapped user information';
 		$result['status']   	=  'Success';
 		$result['statusCode']   =  '200';
 		$result['data']['userList']	= $output;
 	}
 	
 	header('Content-type: application/json');
 	echo  json_encode($result);
 }
 
 public function sendPushMessageToAndroid(){
 	$this->load->model('Pushnotification','',TRUE);
 	$apiKey = 'AIzaSyDZnoLk9EPh2-La3eICXg2xfkKt_OLeovA';
 	$regId = 'APA91bHHgvdapOnOKGKZw-ZB6f8GGumO17MmfVOQDoe5wH74sUA9fKqFN-G45aJ_Ras66Asa4K5S6_v2DoOB5TuqPgFQlJRrSPvV8G51BzZHC3U462i2V8dADbAeM710XQhNqda01yCy';
 	$this->Pushnotification->sendPushMessageToAndroid($apiKey,$regId);
 }
 
}
 
?>
