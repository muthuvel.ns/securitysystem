<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Registration extends CI_Controller {
 
 function __construct() {
   parent::__construct();
   $this->load->helper(array('url','language'));
   $this->load->helper('form');
   $this->load->model('User','',TRUE);
   $this->load->model('Utils','',TRUE);
   $this->load->library('session');
  
   header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
   header("Pragma: no-cache"); // HTTP 1.0.
   header("Expires: 0"); // Proxies.
   
   if ( !isset($this->session->userdata['roleguid']) && empty( $this->session->userdata['roleguid']) ){
   	$this->session->sess_destroy();
   	redirect(base_url().'index.php/login');
   }
 }
 
 function index(){
 	$sessionUserGuid = (!empty( $this->session->userdata['userguid'] ) ? $this->session->userdata['userguid']:'');
 	$message =	'';
 	if (!empty($_REQUEST['email']) && !empty($_REQUEST['pass']) && !empty($_REQUEST['username']) && !empty($_REQUEST['phone']) ){
 		$email	= $_REQUEST['email'];
 		$pass	= $_REQUEST['pass'];
 		$uname	= $_REQUEST['username'];
 		$phone	= $_REQUEST['phone'];
 		
 		$userExists	=	$this->User->userDetails( $email );
 		if ( empty( $userExists )) {
	 		$this->db->trans_begin();
	 		$userGuid  =   $this->Utils->getGuid();
	 		$data = array();
	 		if ( isset( $_FILES['photo']['error'] ) && $_FILES['photo']['error'] == 0) {
		        $config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/";
		        $config['allowed_types'] = '*';
	// 	        $config['max_size'] = '100';
	// 	        $config['max_width']  = '1024';
	// 	        $config['max_height']  = '768';
		        $config['file_name']  = $userGuid.'_'.strtotime(date(DATE_TIME_FORMAT));
		
		        $this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload('photo'))
		        {
		            $error = array('error' => $this->upload->display_errors());
		            $this->load->view('registration/registration_view', $error);
		        }
		        else
		        {
		            $data = array('upload_data' => $this->upload->data());
		        }
	 		}
	           $created		=	date(DATE_TIME_FORMAT);
	            $insertUserData =   array(	'email'				=> $email,
								            'password'			=> md5($pass),
								            'username'			=> $uname,
						            		'created'      		=>  $created,
						            		'created_by'      	=>  $sessionUserGuid,
					               		  	'last_updated'     	=>  $created,
					               		  	'last_updated_by'   =>  $sessionUserGuid,
                                        	'guid'				=>	$userGuid,
							            );
	            $insertUser	    =   $this->User->insertUser($insertUserData);					   	            
	
	            if ( $insertUser ) {
		            $profileGuid  =   $this->Utils->getGuid();
		            $insertProfileData =   array(  
		                                            'user_guid' 		=> $userGuid,
		                                            'phone' 			=> $phone,
		                                            'photo' 			=> (!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:''),
								            		'created'      		=>  $created,
								            		'created_by'      	=>  $sessionUserGuid,
							               		  	'last_updated'     	=>  $created,
							               		  	'last_updated_by'   =>  $sessionUserGuid,
										            'guid'				=>	$profileGuid,
		                                        );
		                                        
		           $insertProfile        =   $this->User->insertUserProfile($insertProfileData);   
	            }
	
	        if (empty( $insertUser ) || empty( $insertProfile ) )   {
	            $this->db->trans_rollback();
	            $message = "Registration Failed";
	        }
	        else
	        {
	        	$message = "Registration Success";
	            $this->db->trans_commit();
	        }
 		}else{
 			$message = 'User Already exists with same email';
 		}
	        $this->session->set_flashdata('message', $message);
	        redirect('index.php/registration/userdetails');
	   }
	   
	   $this->data['url'] =  base_url().'index.php/registration/index';
// echo '<pre>';print_r($message);exit;
   $this->load->view('layouts/header.php');
   $this->load->view('registration/registration_view', $this->data);
   $this->load->view('layouts/footer.php');
 }
 
 function admin( $userRoleGuid ){
 	
  if ( $this->session->userdata['roleguid'] != SUPER_ADMIN_ROLE_ID || $userRoleGuid !=	ADMIN_ROLE_ID){
   	redirect(base_url().'index.php/tracking');
   }
   
   $sessionUserGuid = (!empty( $this->session->userdata['userguid'] ) ? $this->session->userdata['userguid']:'');
   
 	$message =	'';
 	if (!empty($_REQUEST['email']) && !empty($_REQUEST['pass']) && !empty($_REQUEST['username']) && !empty($_REQUEST['phone']) ){
 		$email	= $_REQUEST['email'];
 		$pass	= $_REQUEST['pass'];
 		$uname	= $_REQUEST['username'];
 		$phone	= $_REQUEST['phone'];
 		
 		$userExists	=	$this->User->userDetails( $email );
 		if ( empty( $userExists )) {
	 		$this->db->trans_begin();
	 		$userGuid  =   $this->Utils->getGuid();
	 		$data = array();
	 		if ( isset( $_FILES['photo']['error'] ) && $_FILES['photo']['error'] == 0) {
		        $config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/";
		        $config['allowed_types'] = '*';
	// 	        $config['max_size'] = '100';
	// 	        $config['max_width']  = '1024';
	// 	        $config['max_height']  = '768';
		        $config['file_name']  = $userGuid.'_'.strtotime(date(DATE_TIME_FORMAT));
		
		        $this->load->library('upload', $config);
				
		        if ( ! $this->upload->do_upload('photo')) {
		            $error = array('error' => $this->upload->display_errors());
		            $this->load->view('registration/registration_view', $error);
		        }
		        else{
		            $data = array('upload_data' => $this->upload->data());
		        }
	 		}
	        
            $created		=	date(DATE_TIME_FORMAT);
            $insertUserData =   array(  'email' => $email,
							            'password' => md5($pass),
							            'username' => $uname,
                                        'created'      		=>  $created,
									  	'created_by'      	=>  $sessionUserGuid,
				               		  	'last_updated'     	=>  $created,
				               		  	'last_updated_by'   =>  $sessionUserGuid,
                                        'guid'=>$userGuid,
							            );
            $insertUser	    =   $this->User->insertUser($insertUserData);					   	            

            if ( $insertUser ) {
	            $profileGuid  =   $this->Utils->getGuid();
	            $insertProfileData =   array(  
	                                            'user_guid' 		=> $userGuid,
	                                            'phone' 			=> $phone,
	                                            'photo' 			=> (!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:''),
									            'created'      		=>  $created,
											  	'created_by'      	=>  $sessionUserGuid,
						               		  	'last_updated'     	=>  $created,
						               		  	'last_updated_by'   =>  $sessionUserGuid,
									            'guid'				=>	$profileGuid,
	                                        );
	                                        
	           $insertProfile        =   $this->User->insertUserProfile($insertProfileData);
	           
	           if ( !empty( $insertProfile )){
	           	
		           	$roleGuid  =   $this->Utils->getGuid();
		            $insertRoleData 	=   array(  
		                                            'user_guid' 		=> $userGuid,
		            								'role_guid' 		=> $userRoleGuid,
										            'created'      		=>  $created,
												  	'created_by'      	=>  $sessionUserGuid,
							               		  	'last_updated'     	=>  $created,
							               		  	'last_updated_by'   =>  $sessionUserGuid,
										            'guid'				=>	$roleGuid,
		                                        );
		                                        
		           $insertuserRole        =   $this->User->insertUserRole( $insertRoleData );
	           }
            }
	
	        if (empty( $insertUser ) || empty( $insertProfile ) ||  empty( $insertuserRole ) ) {
	            $this->db->trans_rollback();
	            $message = "Registration Failed";
	        }
	        else {
	        	$message = "Registration Success";
	            $this->db->trans_commit();
	        }
	            
        }else{
        	$message = 'User Already exists with same email';
        }
	        $this->session->set_flashdata('message', $message);
	        redirect('index.php/registration/admindetails/'.ADMIN_ROLE_ID);
	  }
	   
	$this->data['url'] =  base_url().'index.php/registration/admin/'.ADMIN_ROLE_ID; // form submit url
// echo '<pre>';print_r($message);exit;
   $this->load->view('layouts/header.php');
   $this->load->view('registration/registration_view', $this->data);
   $this->load->view('layouts/footer.php');
   
 }
 
 function userDetails(){
 	
 	$message = $this->session->flashdata('message');
 	if ( $message){
 		$this->data['message'] = $message;
 	}
 	
 	/** get user role info */
 	$userRole    =  $this->User->getUserRoleInfo();
 	
 	$inputRole['user_guid']	=	array();
 	if (!empty( $userRole)) {
 		foreach ( $userRole as $role){
 			$inputRole['user_guid'][] =   $role['userGuid'];
 		}
 	}
 	
 	$userGuidList			=	array_unique( $inputRole['user_guid'] );
 	$this->data['user']		=	$this->User->userAndProfileDetails( '', '', $userGuidList );
 	
 	$this->load->view('layouts/header.php');
 	$this->load->view('registration/user_details', $this->data);
 	$this->load->view('layouts/footer.php');
 }
 
 function adminDetails( $userRoleGuid ){

 	if ( $this->session->userdata['roleguid'] != SUPER_ADMIN_ROLE_ID || $userRoleGuid != ADMIN_ROLE_ID){
 		redirect(base_url().'index.php/tracking');
 	}
 	
 	$message = $this->session->flashdata('message');
 	if ( $message){
 		$this->data['message'] = $message;
 	}
 	
 	/** get user role info */
 	$this->data['user']    =  $this->User->getUserAndRoleDetails('', $userRoleGuid);
 
 	$this->load->view('layouts/header.php');
 	$this->load->view('registration/admin_details', $this->data);
 	$this->load->view('layouts/footer.php');
 }
 
 function checkuseremailavailabilty(){
 	
 	if ( isset( $_REQUEST['email'] ) && !empty( $_REQUEST['email'] )) {
 		$exists	=	 $this->User->userDetails( $_REQUEST['email'] );
 		$msg	= 2;
 		if ( !empty( $exists ) ){
 			$msg	=	1;
 		}
 		echo  $msg;
 	}
 }
 
}
 
?>