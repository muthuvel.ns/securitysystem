<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
                                          
class Logout extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
   $this->load->model('User','',TRUE);
   $this->load->model('Login_model','',TRUE);
   $this->load->library('session');
    if ( !isset($this->session->userdata['roleguid']) && empty( $this->session->userdata['roleguid']) ){
   		redirect(base_url().'index.php/login');
   }
 }
 
 function index() {
 	$data	=	array( 'session_id'=>'','email'=>'','roleName'=>'','userguid'=>'','roleguid'=>'','username'=>'','imagepath'=>'');
 	$this->session->unset_userdata($data);
 	$this->session->sess_destroy();
 	redirect(base_url().'index.php/login');
 }
 
}
 
?>