<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Profile extends CI_Controller {
 
 function __construct() {
   parent::__construct();
   $this->load->helper(array('url','language'));
   $this->load->helper('form');
   $this->load->model('User','',TRUE);
   $this->load->model('Utils','',TRUE);
   $this->load->library('session');
  
   header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
   header("Pragma: no-cache"); // HTTP 1.0.
   header("Expires: 0"); // Proxies.
   
   if ( !isset($this->session->userdata['roleguid']) && empty( $this->session->userdata['roleguid']) ){
   	$this->session->sess_destroy();
   	redirect(base_url().'index.php/login');
   }
 }

 function index(){
  
 	$this->data	=	array();
 	$message = $this->session->flashdata('message');
 	if ( $message){
 		$this->data['message'] = $message;
 	}
 	
 	if ( !empty( $_REQUEST['uid'] ) && empty( $_REQUEST['rid'] ) ){
 		$this->data['user']	=	$this->User->userAndProfileDetails($_REQUEST['uid']);
 		$this->data['url']	=	base_url().'index.php/profile/edit?uid='.$_REQUEST['uid'];
 	}elseif ( !empty( $_REQUEST['uid'] ) && !empty( $_REQUEST['rid'] )){
 		$this->data['user']	=	$this->User->userAndProfileDetails($_REQUEST['uid']);
 		$this->data['url']	=	base_url().'index.php/profile/edit?uid='.$_REQUEST['uid'].'&rid='.$_REQUEST['rid'];
 	}
 	
  $this->load->view('layouts/header.php');
  $this->load->view('profile/profile_view.php',$this->data);
  $this->load->view('layouts/footer.php');

 }
 function edit(){
 
 	$sessionUserGuid = (!empty( $this->session->userdata['userguid'] ) ? $this->session->userdata['userguid']:'');
 	if ( !empty( $_REQUEST['uid'] ) ){
	 		$userGuid  =   $_REQUEST['uid'];
	 		
	 		$updateUserData	=	array();
	 		$updateProfileData	=	array();
	 		if ( !empty( $_REQUEST['username'] )) {
	 			$updateUserData['username'] = $_REQUEST['username'];
	 		}
	 		if ( !empty( $_REQUEST['email'] )) {
	 			$updateUserData['email'] = $_REQUEST['email'];
	 		}
	 		if ( !empty( $_REQUEST['pass'] ) && isset( $_REQUEST['pass'] ) && !empty( $_REQUEST['pass_confirmation'] ) && isset( $_REQUEST['pass_confirmation'] )) {
	 			$updateProfileData['password'] = md5($_REQUEST['pass']);
	 		}
	 		if ( !empty( $_REQUEST['phone'] )) {
	 			$updateProfileData['phone'] = $_REQUEST['phone'];
	 		}
	 		
	 		if ( isset( $_FILES['photo']['error'] ) && $_FILES['photo']['error'] == 0) {
	 			$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/";
	 			$config['allowed_types'] = '*';
// 	 			$config['max_size'] = '100';
// 	 			$config['max_width']  = '1024';
// 	 			$config['max_height']  = '768';
	 			$config['file_name']  = $userGuid.'_'.strtotime(date(DATE_TIME_FORMAT));

	 			$this->load->library('upload', $config);
	 			
	 			if ( ! $this->upload->do_upload('photo')) {
	 				$message = $this->upload->display_errors();
 					if ( $this->session->userdata['roleguid'] == ADMIN_ROLE_ID ) {
		 		 		$this->session->set_flashdata('message', $message);
		 		 		redirect('index.php/profile/index?uid='.$_REQUEST['uid'].'&rid='.$_REQUEST['rid']);
		 		 	}
			 		$this->session->set_flashdata('message', $message);
			 		redirect('index.php/profile/index?uid='.$_REQUEST['uid']);
	 			}
	 			else{
	 				$data	= array('upload_data' => $this->upload->data());
	 				$updateProfileData['photo'] =(!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:'');
	 			}
	 		}
 			$this->db->trans_begin();
 			$created 									=   date(DATE_TIME_FORMAT);
 			$updateUserData['last_updated']     		=  $created;
 			$updateUserData['last_updated_by']      	=  $sessionUserGuid;
	 		if ( !empty( $updateUserData ) ) {
	 			$insertUser	    =   $this->User->updateUser($userGuid, $updateUserData);
	 		}
	 		
	 		$updateProfileData['last_updated']     		=  $created;
 			$updateProfileData['last_updated_by']      	=  $sessionUserGuid;
 			if ( !empty( $updateProfileData ) ) {
 				$insertProfile        =   $this->User->updateUserProfile($userGuid, $updateProfileData);
 			}
 		
 		if (empty( $insertUser ) || empty( $insertProfile ) )   {
 			$this->db->trans_rollback();
 			$message = "Update Failed";
 		}
 		else
 		{
 			$message = "Update Success";
 			$this->db->trans_commit();
 		}
 		
 		 if ( !empty( $_REQUEST['rid'] ) ) {
 		 	$this->session->set_flashdata('message', $message);
 		 	if ( $this->session->userdata['roleguid'] == ADMIN_ROLE_ID ) {
 		 		redirect('index.php/profile/index?uid='.$_REQUEST['uid'].'&rid='.$_REQUEST['rid']);
 		 	}
 		 	redirect('index.php/registration/admindetails/'.$_REQUEST['rid']);
 		 }else{
 		 	$this->session->set_flashdata('message', $message);
 		 	if ( $this->session->userdata['roleguid'] == ADMIN_ROLE_ID ) {
 		 		redirect('index.php/profile/index?uid='.$_REQUEST['uid'].'&rid='.$_REQUEST['rid']);
 		 	}
	 		redirect('index.php/registration/userdetails');
 		 }
 	}
 	
 }
 
  
}


?>
