<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Notified extends CI_Controller {
 
 function __construct() {
   parent::__construct();
   parent::__construct();
   $this->load->model('Notification_model','',TRUE);
   $this->load->model('Tracking_model','',TRUE);
   $this->load->model('Usermanager','',TRUE);
   $this->load->model('User','',TRUE);
   $this->load->library('session');
   
   header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
   header("Pragma: no-cache"); // HTTP 1.0.
   header("Expires: 0"); // Proxies.
   
   if ( !isset($this->session->userdata['roleguid']) && empty( $this->session->userdata['roleguid']) ){
   	$this->session->sess_destroy();
   	redirect(base_url().'index.php/login');
   }
  
 }

 function index(){
 	$userData		=	$this->User->userTrackingDetails();
 	
 	$userGuidList	=	array();
 	$deviceIdList	=	array();
 	$data			=	array();
 	if ( !empty( $userData )) {
 		foreach ( $userData as $value){
 			$data['userGuid'][]	=	$value['user_guid'];
 			$data['DeviceId'][]	=	$value['device_id'];
 		}
 		
 		$userGuidList	=	array_unique($data['userGuid']);
 		$deviceIdList	=	array_unique($data['DeviceId']);
 	}
 	
 	
 	if ( !empty( $userGuidList ) ) {
 		$data['user'] = $this->User->userAndProfileDetails( $userGuidList );
 	}
 	
 	if ( !empty( $deviceIdList ) ) {
 		$data['device'] = $this->Tracking_model->getdeviceInfo( '', $deviceIdList );
 	}
 	
//  	echo '<pre>';print_r($this->data);exit;
 //	$notification	=	$this->Notification_model->getNotificationDetails();
 	
  $this->load->view('layouts/header.php');
  $this->load->view('notified/notified_view.php', $data);
  $this->load->view('layouts/footer.php');

 }
 
 function device(){

 	if ( !empty( $_REQUEST['dev_id'] ) ) {
 		$this->data['notification']	=	$this->Notification_model->getNotificationDetails( '',$_REQUEST['dev_id'] );
 		$this->data['device'] = $this->Tracking_model->deviceInfo( $_REQUEST['dev_id'] );
	}
  $this->load->view('layouts/header.php');
  $this->load->view('notified/device_notify_view.php', $this->data);
  $this->load->view('layouts/footer.php');
 }

 function user_notify(){
 	if ( !empty( $_REQUEST['uid'] ) ) {
 		$this->data['notification']	=	$this->Notification_model->getNotificationDetails( $_REQUEST['uid'] );
 		$this->data['user'] = $this->User->userAndProfileDetails( $_REQUEST['uid'] );
 	}
//  	echo '<pre>';print_r($this->data);exit;
  $this->load->view('layouts/header.php');
  $this->load->view('notified/user_notify_view.php', $this->data);
  $this->load->view('layouts/footer.php');
 }


}


?>
