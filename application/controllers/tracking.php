<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Tracking extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
   $this->load->database();
   $this->load->helper(array('url','language'));
   $this->load->model('Tracking_model','',TRUE);
   $this->load->model('User','',TRUE);
   $this->load->model('Usermanager','',TRUE);
   $this->load->model('Utils','',TRUE);
   $this->load->model('Trackingmanager','',TRUE);
   $this->load->model('Pushnotification','',TRUE);
   $this->load->helper('form');
   $this->load->library('session');
  
   header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
   header("Pragma: no-cache"); // HTTP 1.0.
   header("Expires: 0"); // Proxies.
   
   if ( !isset($this->session->userdata['roleguid']) && empty( $this->session->userdata['roleguid']) ){
   		$this->session->sess_destroy();
   		redirect(base_url().'index.php/login');
   }
   
 }
 
 function index( ) {
 	$message = $this->session->flashdata('message');
 	if ( $message){
 		$this->data['message'] = $message;
 	}
	 	
 	$trakingIdList	= array();
 	$userId		=	(!empty( $_REQUEST['uid'] ) ? $_REQUEST['uid'] : '');
 	if ( !empty( $userId ) ){
 		$userTracking  =   $this->User->userTrackingDetails('','',$userId);
 	}else{
 	    $userTracking  =   $this->User->userTrackingDetails();
 	}
 	
 	if (!empty( $userTracking ) ) {
 		foreach ( $userTracking as $user) { 
 			$flag =0;
 			/** validate deviceID is exits or not. if exits add the list otherwise omited */
 			$userProfile  =   $this->User->userProfileDetails('', $user['device_id']);
 			$trackingInfo =   $this->Tracking_model->deviceInfo($user['device_id']);
 			
 			if ( !empty( $userProfile )) {
 				$flag =1;
 			}
 			
 			if ( !empty( $trackingInfo )) {
 				$flag =1;
 			}
 			
 			if ( $flag ==1){
 				$trakingId[$user['user_guid']][] = $user['guid'];
 			}
 		}
 		/** get user based count list .User have two trackingId then it'll show for map otherwise remove the tracking */
 		$list = array();
 		if ( !empty( $trakingId ) ) {
 			foreach ( $trakingId as $id) {
 				if ( count($id) == 2){
 					$list[] = $id;
 				}
 			}
			/** two dimentional to single dimention array conversion*/
 			$trakingIdList = call_user_func_array('array_merge', $list);
 		}
 	}
 	
 	$this->data	=	$this->Trackingmanager->getDeviceInfo( $trakingIdList );
 	
    if ( isset($_REQUEST['ajax'])){
        $result  =   array(	'waypoints'=>(!empty( $this->data['waypoints'] )?$this->data['waypoints']:0),
        					'points'=>(!empty( $this->data['points'] )?$this->data['points']:0),
        					'marker'=>(!empty( $this->data['marker'] )?$this->data['marker']:0)
					        );
        echo  json_encode($result); exit;
    }
    $this->load->view('layouts/header.php');
    $this->load->view('tracking/index', $this->data);
    $this->load->view('layouts/footer.php');
 }
 
 function setroute(){
 	$message = $this->session->flashdata('message');
 	if ( $message){
 		$info['message'] = $message;
 	}
 	  $info   =   $this->Usermanager->getActiveUserAndDeviceInfo();
 	  $this->load->view('layouts/header.php');
 	  $this->load->view('tracking/set_route', $info);
 	  $this->load->view('layouts/footer.php');
 }
 

 function insertlocation(){
 	
 	$flag =   0;
 	$message='';
 	if (!empty($_REQUEST['device_id']) && !empty($_REQUEST['user_id']) && !empty($_REQUEST['start_lat']) && !empty($_REQUEST['start_lng'])&& !empty($_REQUEST['end_lat']) && !empty($_REQUEST['end_lng']) && !empty( $_REQUEST['hour']) && !empty( $_REQUEST['min'])) {
 		
 	      $this->db->trans_begin();
	
 	      /** assgning value */
 	      $deviceId    =   $_REQUEST['device_id'];
 	      $userGuid    =   $_REQUEST['user_id'];
 	      $start_lat   =   $_REQUEST['start_lat'];
          $start_lng   =   $_REQUEST['start_lng'];
          $end_lat     =   $_REQUEST['end_lat'];
          $end_lng     =   $_REQUEST['end_lng'];
          $hour			=   $_REQUEST['hour'];
          $min	     	=   $_REQUEST['min'];
          $source		=	(!empty($_REQUEST['source'])?$_REQUEST['source']:'Unknown');
          $destination	=	(!empty($_REQUEST['destination'])?$_REQUEST['destination']:'Unknown');
		  $sessionUserGuid = (!empty( $this->session->userdata['userguid'] ) ? $this->session->userdata['userguid']:'');
          $insertUserTracking	= 0;
          $insertWayPoint		= 0;
          $insertTrackPoints	= 0;
          
          $userExists = $this->User->userTrackingDetails( '', $deviceId, $userGuid );
 	      $deviceData = array();
 	      
 	      $created		=	date(DATE_TIME_FORMAT);
 	      if (empty( $userExists ) ) {
 	      	   $deviceGuid  =   $this->Utils->getGuid();
 	      	   $deviceData[]   =   array( 'device_id'     		=>  $deviceId,
							 	      	  'user_guid'   		=>  $userGuid,
							 	      	  'created'      		=>  $created,
										  'created_by'      	=>  $sessionUserGuid,
					               		  'last_updated'      	=>  $created,
					               		  'last_updated_by'     =>  $sessionUserGuid,
							 	      	  'guid'        		=>  $deviceGuid
		 	      	                    );
 	      	 
 	      }
 	      if(!empty( $deviceData )){
	 	      $userProfile    =   $this->User->userProfileDetails( $userGuid );
	 	      if ( !empty( $userProfile[0]['device_id'] )){
		 	      $userdeviceId   =  $userProfile[0]['device_id'] ;
		 	     
		 	      $userExists1 = $this->User->userTrackingDetails( '', $userdeviceId, $userGuid );
		 	      $userDeviceData = array();
		 	      if (empty( $userExists1 ) ) {
		               $userDeviceGuid  =   $this->Utils->getGuid();
		               $userDeviceData[]   =   array(     'device_id'   		=>  $userdeviceId,
					                                      'user_guid'    		=>  $userGuid,
					                                      'created'      		=>  $created,
														  'created_by'      	=>  $sessionUserGuid,
									               		  'last_updated'      	=>  $created,
									               		  'last_updated_by'     =>  $sessionUserGuid,
					                                      'guid'         		=>  $userDeviceGuid
				                                   );
		          }
		          
		          if ( !empty( $deviceData ) && !empty( $userDeviceData )) {
		 	          $data   =   array_merge($deviceData,$userDeviceData );
		 	          $insertUserTracking  =   $this->User->insertUserTrackingInfo($data);
		          }
		          
			 	    // $trackExists = $this->Tracking_model->trackingPoints( $_REQUEST['track_id'] );
		 	    if ( !empty( $insertUserTracking ) ) {
		 	        /** insert tracking points */
			 	    //$time = $this->Tracking_model->calculateStartAndEndPoints($start_lat, $start_lng, $end_lat, $end_lng);
		            $startDate 		= date('Y-m-d H:i:s');
			 	    $time			= $hour.' hours '.$min.' mins';
			 	    $newtimestamp 	= strtotime("$startDate + $time" );
			 	    $endDate		= date('Y-m-d H:i:s', $newtimestamp);
			 	    
			 	    /* if ( !empty( $time )) {
				 	    $newtimestamp = strtotime("$startDate + $time" );
			            $endDate= date('Y-m-d H:i:s', $newtimestamp);
			 	    } */
			 	    
		            $track_points[0] = array(
		                                'tracking_id'  =>$deviceGuid,
		                                'start_lat'    =>$start_lat,
							            'start_lng'    =>$start_lng,
					            		'source'   		=>$source,
					            		'destination'   =>$destination,
							            'end_lat'      =>$end_lat,
							            'end_lng'      =>$end_lng,
		                                 'start_date'   =>$startDate,
		                                 'end_date'     =>$endDate
		                                 );
		          $track_points[1] = array(
                                        'tracking_id'  =>$userDeviceGuid,
                                        'source'   		=>$source,
					            		'destination'   =>$destination,
						          		'start_lat'    =>$start_lat,
						          		'start_lng'    =>$start_lng,
                                        'end_lat'      =>$end_lat,
                                        'end_lng'      =>$end_lng,
                                         'start_date'   =>$startDate,
                                         'end_date'     =>$endDate
                                         );
		          
		            $insertTrackPoints = $this->Tracking_model->insertTrackingPoints( $track_points );
		           
		            if ( $insertTrackPoints ){
		            	$time    =   WAYPOIND_TIMING;
		            	$newtimestamp = strtotime("$startDate + $time" );
                        $endDate= date('Y-m-d H:i:s', $newtimestamp);
                    
		            	$way_points[0] = array(
                                        'tracking_id'  =>$deviceGuid,
                                        'lat'    =>$start_lat,
                                        'lng'    =>$start_lng,
                                         'start_date'   =>$startDate,
                                         'end_date'     =>$endDate,
		            	                 'status'     =>1
                                         );
                         $way_points[1] = array(
                                        'tracking_id'  =>$userDeviceGuid,
                                        'lat'    =>$start_lat,
                                        'lng'    =>$start_lng,
                                         'start_date'   =>$startDate,
                                         'end_date'     =>$endDate,
                                        'status'     =>1
                                         );
                                         
		            	$insertWayPoint = $this->Tracking_model->insertWayPoints( $way_points );
		            }else{
		            	$message	=	"Tracking point does't not insert";
		            }
		 	     }else{
		 	     	$message	=	"User Tracking point does't not insert";
		 	     }
	 	      }else{
	 	      	$message	=	'deviceID not found for this user';
	 	      }
 	      }else{
 	      	$message	=	'deviceID and UserID already Used';
 	      }
 	      
		if ($this->db->trans_status() === FALSE)
		{
		    $this->db->trans_rollback();
		    $message	=	'Route not set please try agin after some time!';
		}
		else
		{
			if (empty($insertUserTracking) || empty( $insertTrackPoints ) || empty( $insertWayPoint )){
				$this->db->trans_rollback();
				$message	=	'Route not set please try agin after some time';
			}else {
			    $this->db->trans_commit();
			    $flag 	=   1;
			   $message	=	'Successfully Added';
			}
        }
		    
 	}
 	
 	if ( $flag ) {
 		$this->session->set_flashdata('message', $message);
 		//$this->data['message']	=	$this->session->flashdata('message');
 		redirect('index.php/tracking/index');
 	}else {
 		$this->session->set_flashdata('message', $message);
 		redirect('index.php/tracking/setroute');
 	}
 }
 
 public function ajaxcall(){
 	$insertWayPoints =0;
	 /*if ( isset($_REQUEST['ajax'])){
	        $insert   =   $this->Tracking_model->wapointData();
	        if (isset($insert[$_REQUEST['ajax']])) {
	            $data= $insert[$_REQUEST['ajax']];
	            $data['tracking_id']=12345678;
	            $data['date']=date('Y-m-d H:i:s');
	            $data['status']=1;
	            
	              /** update tracking way points  status = 0  based on tracking id*/
	/*                $waypointExists = $this->Tracking_model->wayPoints( $data['tracking_id'] );
	                
	                if ( !empty( $waypointExists )){
	                    $track_status = array(
	                                            'status'=>'0'
	                                         );
	                    $update_status = $this->Tracking_model->updateWayPoints( $data['tracking_id'], $track_status );
	                }
	               
	            $insertWayPoints = $this->Tracking_model->insertWayPoints($data);
	        }
	    }*/
	    echo $insertWayPoints;exit;
 }
 
  function generatedevice(){
  	
  	$message = $this->session->flashdata('message');
  	if ( $message){
  		$this->data['message'] = $message;
  	}
  	
  	$msg = '';
 	if (!empty( $_REQUEST['device_id'] ) && !empty( $_REQUEST['device_name'] ) && isset( $_REQUEST['type'] )) {
	    $deviceGuid =   $_REQUEST['device_id'];
	    $deviceExists=	$this->Tracking_model->deviceInfo( $deviceGuid );
	    $exists		=	$this->Tracking_model->deviceInfo( $deviceGuid );
	    
	    $sessionUserGuid = (!empty( $this->session->userdata['userguid'] ) ? $this->session->userdata['userguid']:'');
	    $created		=	date(DATE_TIME_FORMAT);
	    $deviceData 	= array(	
	    							'name' 				=>	$_REQUEST['device_name'],
								    'last_updated'      =>  $created,
								    'last_updated_by'   =>  $sessionUserGuid
							    );
	    
	    if ( !empty( $exists ) && $_REQUEST['type'] == 'up') {
	    	$deviceData['created']     		=  $created;
	    	$deviceData['created_by']      	=  $sessionUserGuid;
	    	/** update */
	    	$update 		= $this->Tracking_model->updateTrackingInfo( $deviceGuid, $deviceData );
	    	$msg = 'Successfully updated';
	    }else{
	    	
	    	if ( !empty( $exists ) &&  $_REQUEST['type'] =='add') {
	    		$msg = 'Device id already exists';
	    	}elseif (empty( $exists ) &&  $_REQUEST['type'] =='add'){
	    		/** insert */
	    		$pwd         				=   $this->Utils->getRandomString();
	    		$deviceData['device_id']	= $deviceGuid;
	    		$deviceData['password']		= $pwd ;
	    		$insert = $this->Tracking_model->insertTrackingInfo( $deviceData );
	    		$msg = 'Added successfully';
	    	}
	    }
	    $this->session->set_flashdata('message', $msg); 
 	}
 	
 	$this->data['deviceDetails']	=	$this->Tracking_model->deviceInfo();
 	
 	$this->load->view('layouts/header.php');
 	$this->load->view('tracking/device_generation', $this->data);
 	$this->load->view('layouts/footer.php');
    
 }
 
 function individualmap(){
 	
 }
 
 function details(){
 	
 	$message = $this->session->flashdata('message');
 	if ( $message){
 		$info['message'] = $message;
 	}
 	
 	$info   =   $this->Usermanager->getEnableUserAndDeviceInfo();
 	$this->load->view('layouts/header.php');
 	$this->load->view('tracking/details', $info);
 	$this->load->view('layouts/footer.php');
 }
 
 function disabledevice(){
 	
 	$message 		= '';
 	$url			= 0;
 	$result 		= 0;
 	$table			=	array();
 	$sessionUserGuid = (!empty( $this->session->userdata['userguid'] ) ? $this->session->userdata['userguid']:'');
 	$created		 =	date(DATE_TIME_FORMAT);
 	$userTrackingDetails = array();
 	
 	if ( isset( $_REQUEST['uid'] ) && !empty( $_REQUEST['uid'] ) ) {
 		$userTrackingDetails	=	$this->User->userTrackingDetails('', '', $_REQUEST['uid'] );
 		
 		$url	=	'index.php/tracking/details';
 	}elseif ( isset( $_REQUEST['dev_id'] ) && !empty( $_REQUEST['dev_id'] ) ){
 		/** get device info */
//  		$trackinginfo		=	$this->Tracking_model->deviceInfo( $_REQUEST['dev_id'] );
 		/** get device with user info */
 		$userTracking		=	$this->User->userTrackingDetails( '', $_REQUEST['dev_id'] );
 		
 		if(!empty( $userTracking[0]['user_guid'] )){
 			/** get userguid based on device details */
 			$userTrackingDetails		=	$this->User->userTrackingDetails('', '', $userTracking[0]['user_guid'] );
 		}
 		$table['tracking_info']		= array('fieldname'=>'device_id','fieldvalue'=>$_REQUEST['dev_id'] );
 		
 		$url	=	'index.php/tracking/generatedevice';
 	}
 		if ( !empty( $userTrackingDetails )) {
	 		
	 		foreach ( $userTrackingDetails as $user) {
	 			$deviceDetails	=	$this->Tracking_model->deviceInfo( $user['device_id'] );
	 			if ( !empty( $deviceDetails )) {
	 				$data['device_id'][]	=	$user['device_id'];
	 			}
	 			$data['tracking_id'][]	=	$user['guid'];
	 		}
	 		
// 	 		$table['tracking_info']		= array('fieldname'=>'device_id','fieldvalue'=>$data['device_id'] );
	 		$table['user_tracking']		= array('fieldname'=>'guid','fieldvalue'=>$data['tracking_id'] );
	 		$table['tracking_points']	= array('fieldname'=>'tracking_id','fieldvalue'=>$data['tracking_id'] );
	 		$table['waypoints']			= array('fieldname'=>'tracking_id','fieldvalue'=>$data['tracking_id'] );
 		}
 		
 		if ( isset( $_REQUEST['user']) && $_REQUEST['user'] == 'user') {
 			$table['user']				= array('fieldname'=>'guid','fieldvalue'=>$_REQUEST['uid'] );
 			$table['user_profile']		= array('fieldname'=>'user_guid','fieldvalue'=>$_REQUEST['uid'] );
 			$url	=	'index.php/registration/userdetails';
 		}
 		
 		if ( isset( $_REQUEST['user']) && $_REQUEST['user'] == 'admin') {
 			$table['user']				= array('fieldname'=>'guid','fieldvalue'=>$_REQUEST['uid'] );
 			$table['user_profile']		= array('fieldname'=>'user_guid','fieldvalue'=>$_REQUEST['uid'] );
 			$table['user_role']			= array('fieldname'=>'user_guid','fieldvalue'=>$_REQUEST['uid'] );
 			$url	=	'index.php/registration/admindetails/'.ADMIN_ROLE_ID;
 		}
 		
 		if ( !empty( $table )) {
	 		foreach ( $table as $key=>$value ) {
	 			if ( $key == 'user' || $key == 'user_profile' || $key == 'user_role' || $key == 'tracking_info' || $key == 'user_tracking' ) {//echo '<pre>';print_r($key);
	 				$updateData['deleted']			 =	 1;
	 				$updateData['last_updated']      =  $created;
	 				$updateData['last_updated_by']   =  $sessionUserGuid;
	 			}else{
	 				$updateData		=	array( 'deleted' => 1);
	 			}
	 			$result		=	$this->Tracking_model->disableGivenDetails( $key, $value['fieldname'], $value['fieldvalue'], $updateData );
	 		}
 		}
 			$message = 'Update Failure';
 		if ( !empty( $result ) ) {
 			$message = 'Update Successfully';
 		}
 	
 		$this->session->set_flashdata('message', $message);	
 		if ( $url ) {
 			redirect($url);
 		}
 		
 		redirect('index.php/tracking/details');
 }
 
 function notification(){
 	$apiKey = 'AIzaSyCOaR3eC5PMA4C1qsZ71GWllUZ7aEhoLRw';
 	$regId = 'APA91bFE9SMl-uxgQNMV3zJB0M54XDhJTnVIQKTZmn4-fDBxdyORbe2mK9zDYENwGKFNep_Qvka7Km7rQdB7LNEKyKcm6Y2kI8P8b6IeM8bcwBrfrFQmMRTm5f04eNYLmn8cl39DBxhF';
 	
 	$result = $this->Pushnotification->sendPushMessageToAndroid($apiKey, $regId);
 	echo '<pre>';print_r($result);exit;
 }
 
}
 
?>
