<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
                                          
class Login extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
   $this->load->model('User','',TRUE);
   $this->load->model('Login_model','',TRUE);
   $this->load->library('session');
   
   header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
   header("Pragma: no-cache"); // HTTP 1.0.
   header("Expires: 0"); // Proxies.
   
    if ( isset($this->session->userdata['roleguid']) && !empty( $this->session->userdata['roleguid']) ){
   	redirect(base_url().'index.php/tracking');
   } 
 }
 
 function index() {
 	$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
 	$this->output->set_header("Pragma: no-cache");
 	
 	$this->data['message']	=	'';
 	if( $this->input->post() ) {
 		$_REQUEST['username']	= strtolower($_REQUEST['username']);
 		if($this->isValidLoginForm($_REQUEST)) {
 			$formvalues	=	$this->input->post();
 			$userdata	=	$this->Login_model->loginValidation( $formvalues );
 			if ( !empty( $userdata ) )	{
	 			/** check email count is one then redirect to crossponding user domain*/
	 			if( count($userdata) == 1 ) {/** User has one role */
	 				/** Get user name  */
	 				$userProfileName	= ucfirst($userdata['0']['username']);
	 				$userProfile		= $this->User->userProfileDetails( $userdata['0']['userGuid'] );
	 				$imagePath			= (!empty( $userProfile['0']['photo'] ) ? $userProfile['0']['photo']:'');
	 	
	 				/** Set session value  */ 
	 				$data['email']				= $userdata['0']['email'];
	 				$data['roleName']			= $userdata['0']['roleName'];
	 				$data['userguid']			= $userdata['0']['userGuid'];
	 				$data['roleguid']			= $userdata['0']['userRoleGuid'];
	 				$data['username']			= $userProfileName;
	 				$data['imagepath']			= $imagePath;
	 	
	 				$this->session->set_userdata($data);
	 	
	 				/** Login activity  */
	 				$loginActivity	= $this->Login_model->loginActivityEntry($userdata['0']['userGuid'], 1, 'Cloud');
	 	
	 				/** Get redirection path by passing user roleguid  */
	 				$loginPath		= $this->Login_model->loginRedirection($userdata['0']['userRoleGuid']);//echo '<pre>';print_r($loginPath);exit;
	 				redirect($loginPath);
	 			}
 			}else{
 				$this->data['message']	=	'Username or Password you entered is incorrect';
 			}
 		}else{
 			$this->data['message']	=	'Invalid Email Address';
 		}
 	}
   $this->load->view('login/login', $this->data);
 }
 
 
 function isValidLoginForm( $request ) {
 	/** Check email structure validation  */
 	return preg_match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$^', $request['username']);
 }
 
 
}
 
?>