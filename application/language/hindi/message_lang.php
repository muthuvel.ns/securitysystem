<?php

/*
 * French language
 */

$lang['text_success_update'] = "सफलतापूर्वक उत्परिवर्तित"; 
$lang['text_success_insert'] = "सफलतापूर्वक उत्परिवर्तित"; 
$lang['text_already_exists'] = "सफलतापूर्वक उत्परिवर्तित"; 
$lang['text_security_system'] = "&#2360;&#2369;&#2352;&#2325;&#2381;&#2359;&#2366"; 
$lang['text_rest_invalid_credentials'] = 'Authentification invalide';
$lang['text_rest_ip_denied'] = 'IP refusée';
$lang['text_rest_ip_unauthorized'] = 'IP non-autorisée';
$lang['text_rest_unauthorized'] = 'Non autorisé';
$lang['text_rest_ajax_only'] = 'Seul les requêtes AJAX sont autorisées';
$lang['text_rest_api_key_unauthorized'] = 'Cette clef d\'API n\'a pas accès au contrôleur demandé';
$lang['text_rest_api_key_permissions'] = 'Cette clef d\'API n\'a pas les permissions requises';
$lang['text_rest_api_key_time_limit'] = 'Cette clef d\'API a atteint sa limite de temps pour cette méthode';
$lang['text_rest_unknown_method'] = 'Méthode inconnue';
$lang['text_rest_unsupported'] = 'Protocole non-supporté';
