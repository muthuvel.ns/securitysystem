<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/ico/fav_icon.png">
    <title>Login</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>assets/css/pages/signin.css" rel="stylesheet" type="text/css">

</head>
<script>
		function disableBackButton()
		 {
		   setTimeout("preventPageLoad()", 1);
		 }
		 
		 function preventPageLoad()
		 {
		   try {
		     history.forward();
		   } catch (e) {   
		   }
		   // Try again every 200 milisecs.
		   setTimeout("preventPageLoad()", 200);
		 }
</script>
<body>
	
<!--	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.html">
				Bootstrap Admin Template				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<li class="">						
						<a href="signup.html" class="">
							Don't have an account?
						</a>
						
					</li>
					
					<li class="">						
						<a href="index.html" class="">
							<i class="icon-chevron-left"></i>
							Back to Homepage
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
<!--		</div> <!-- /container -->
		
<!--	</div> <!-- /navbar-inner -->
	
<!-- </div> <!-- /navbar -->



<div class="account-container">
	<div class="content clearfix">
		
		<form action="<?php echo base_url(); ?>index.php/login" method="post"> 
		
			<h1 class="logo"><img src="<?php echo base_url();?>assets/img/196X70.png">	</h1>	
			<?php if ( !empty( $message ) ) { ?>
					<div class="msg error-msg" style="text-align: center;"><?php echo $message;?></div>
			<?php }?>
			<div class="login-fields">
				
				<p></p></br>
				
				<div class="field">
					<label for="username">Username</label>
					<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field icon-user" data-validation-optional= "false" data-validation="email" data-validation-error-msg="Please enter the valid email"/>
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field" data-validation="required" data-validation-error-msg="Please enter the password"/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<span class="login-checkbox">
					<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
				</span>
						<!--<input type="submit" class="button btn btn-success btn-large" name ="login" value="Sign In"/>	-->	
				 <button class="button btn btn-success btn-large">Sign In</button> 
				
			</div> <!-- .actions -->
			
		</form>
		
	</div> <!-- /content -->
	<div class="login-extra">
<p> Copyright &copy; 2016 <a target="_blank" href="#" title="Security System">securitysystem.com</a>  All rights reserved.</p>
</div>
</div> <!-- /account-container -->

</div>
	

<!--div class="login-extra">
	<a href="#">Reset Password</a>
</div--> <!-- /login-extra -->
<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>assets/js/signin.js"></script>

<script>
// Setup form validation
$.validate({
	 onError : function() {
		 $(":input.error:first").focus();
		 return false;
	    },
});
</script>

</body>

</html>
