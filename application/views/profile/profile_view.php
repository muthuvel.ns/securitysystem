<link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/register.js"></script>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       <div id="edit_profile"><h2>Profile</h2></div>
       		 <?php if (!empty($message)) : ?>
					<script>
						$(function () {
							 $('#success_msg').slideDown();
								setTimeout(function() {
						 			 $('#success_msg').slideUp();
								}, 3000);
						});
					</script>
					<div class="alert_wrapper" id="success_msg" style="display:block;">
						<div class="alert alert-large alert-success"><button class="close" data-dismiss="alert"></button><center><?php echo $message; ?></center></div>
						<a href="javascript:;"  onclick="$('.sucessfull-message').hide();"></a>
					</div>
				<?php endif; 
				
	if ( !empty( $user ) ) {
		foreach ( $user as $key=>$value ) {
			$image	=	(!empty($value['photo'])? base_url().'assets/upload_images/'.$value['photo']:base_url().'assets/upload_images/'.'default.jpeg');
			$uname	=	(!empty( $value['username'] ) ? $value['username'] : '');
			$email	=	(!empty( $value['email'] ) ? $value['email'] : '');
			$phone	=	(!empty( $value['phone'] ) ? $value['phone'] : '');
	?>
	<div class="col-md-6 profileview" id="profile">
		 <style>
			.table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
			background-color: rgba(41, 128, 185, 0.65);
			}
		</style>
          <div class="col-md-12">
            <span class="profileimg"><img src="<?php echo $image ?> " width="100" height="100" class="pro" ></span>
            <!-- <a href="<?php //echo $url;?>" class="btn btn-primary profile" id="edit_btn">Edit Profile</a>-->
            <button class="btn btn-primary profile" id="edit_btn">Edit Profile</button>
          </div>

         <div class="supplierprofile">

          <table class="table table-striped user">   
       
            <tr>
              <th>User Name</th><td><?php echo $uname;?></td>
            </tr>
            <tr>
              <th>Email</th><td><?php echo $email;?></td>
            </tr>
            <tr>
              <th>Mobile  </th><td><?php echo $phone;?></td>
            </tr>
           </div>
          </table>
        </div>
      </div>
      
		     <div class="col-sm-6" id="edit" style="display: none;">
		     <div class="regform">
		  <?php echo form_open_multipart($url, array('id' => 'myform' , 'class' => 'regformcon'));?>
		    
		     			<fieldset class="form-group">
		                <label for="Username">Username</label>
		                <input type="text" placeholder="User Name" maxlength="50" data-validation="alphanumeric" data-validation-error-msg="Please Enter valid  name" data-validation-allowing="-_ " data-validation-optional="false" class="form-control" name="username" id="username" value="<?php echo $uname;?>">
		              </fieldset>
					
					<div id="emailid">
		              <fieldset class="form-group">
		                <label for="email">Email</label>
		                <input type="text" readonly placeholder="Email" onblur="emailavailablility()" focus="" data-validation-error-msg="Please Enter valid e-mail" data-validation-length="max72" data-validation="email" data-validation-optional="false" class="form-control" name="email" id="email" value="<?php echo $email;?>">
		              </fieldset>
					</div>
					<?php  if ( !empty( $_REQUEST['rid'])) {?>
					<fieldset class="form-group">
		                <label for="repassword">Password <a id="flip">(click)</a></label>
		              </fieldset>
		              
					<div class="panel">
		              <fieldset class="form-group">
		                <label for="Password">Password</label>
		                <input type="password" placeholder="Password" data-validation-length="min6" data-validation-error-msg="You can not leave this field as empty" data-validation="length" data-validation-optional="true" class="form-control" name="pass_confirmation" id="pass_confirmation" value="">
		              </fieldset>
		
		              <fieldset class="form-group">
		                <label for="repassword">Confirm Password</label>
		                <input type="password" placeholder="Confirm Password" data-validation-error-msg="Password are mismatch" data-validation="confirmation" data-validation-optional="true" class="form-control" name="pass" id="rpass" value="">
		              </fieldset>
		              </div>
					<?php } ?>
		
		              <fieldset class="form-group">
		                <label for="phone">Phone/Mobile Number</label>
		                <input type="text" placeholder="Phone/Mobile Number" maxlength="12" data-validation-error-msg="Enter a valid number(ex:50)" data-validation="number" data-validation-optional="false" class="form-control" name="phone" id="phone" value="<?php echo $phone;?>">
		              </fieldset>
		
					<fieldset class="form-group">
		                <label for="photo">Photo</label>
		                <div><span style="float:left;"><img alt="" src="<?php echo $image ?>" id="blah" width="100" height="100"></span></div>
		                 <div class="fileUpload btn btn-primary">
		                 <i style="font-size: 18px;margin-right: 8px;" class="icon-upload-alt"></i><span>Upload</span>
		                 <input type="file" name="photo" id="photo" onchange="readURL(this);" class="upload" value="<?php (!empty( $value['photo'] ) ?$value['photo'] : '');?>">
		                </div>
		              </fieldset>
		
		              <fieldset class="form-group">
		              <button type="submit" class="btn btn-primary">Save Changes</button>
		              </fieldset>
		    </form>
		    </div><!-- regform -->
		    </div>
    
     <?php } 
		} else {?><h2>Details Not Found</h2><?php } ?>
     
 <style type="text/css">

div.panel {

display: none;
}
</style>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
<script src="<?php echo base_url();?>assets/js/form-validator/security.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$("#flip").click(function() {
$(".panel").slideToggle("slow");
});

$('#pass_confirmation').val('');
$('#rpass').val('');

});
// just for the demos, avoids form submit

/*$( "#myform" ).validate({
  rules: {
    password: "required",
    repassword: {
      equalTo: "#password"
    }
  }
});*/

$( "#edit_btn" ).click(function() {
	$('#profile').hide();
	$('#edit').show();
	$('#edit_profile').html('<h2>Edit Profile</h2>');
});

	

$.validate({
	  modules : 'security',
	  onError : function() {
			 $(":input.error:first").focus();
			 return false;
		    },
    onValidate : function() {
		 errortext	=	$("#email").attr('current-error');
		 if(errortext!='') {
			  return {
		        element : $('#email'),
		        message : errortext,
		      }
		 }
		 
	    },
	});
	
function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#blah')
                   .attr('src', e.target.result)
                   .width(100)
                   .height(100);
           };

           reader.readAsDataURL(input.files[0]);
       }
   }
</script>




