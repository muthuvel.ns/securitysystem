<link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/register.js"></script>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h1 class="title">Registration</h1>

           <div class="regform">
  <?php echo form_open_multipart($url, array('id' => 'myform' , 'class' => 'regformcon'));?>
    
     		 <fieldset class="form-group">
                <label for="Username">Username</label>
                <input type="text" placeholder="User Name" maxlength="50" data-validation="alphanumeric" data-validation-error-msg="Please Enter valid  name" data-validation-allowing="-_ " data-validation-optional="false" class="form-control" name="username" id="username" value="">
              </fieldset>
			
			<div id="emailid">
              <fieldset class="form-group">
                <label for="email">Email</label>
                <input type="text" placeholder="Email" onblur="emailavailablility()" focus="" data-validation-error-msg="Please Enter valid e-mail" data-validation-length="max72" data-validation="email" data-validation-optional="false" class="form-control" name="email" id="email" value="">
              </fieldset>
			</div>
              <fieldset class="form-group">
                <label for="Password">Password</label>
                <input type="password" placeholder="Password" data-validation-length="min6" data-validation-error-msg="You can not leave this field as empty" data-validation="length" data-validation-optional="false" class="form-control" name="pass_confirmation" id="pass_confirmation" value="">
              </fieldset>

              <fieldset class="form-group">
                <label for="repassword">Confirm Password</label>
                <input type="password" placeholder="Confirm Password" data-validation-error-msg="Password are mismatch" data-validation="confirmation" data-validation-optional="false" class="form-control" name="pass" id="rpass" value="">
              </fieldset>


              <fieldset class="form-group">
                <label for="phone">Phone/Mobile Number</label>
                <input type="text" placeholder="Phone/Mobile Number" maxlength="12" data-validation-error-msg="Enter a valid number(ex:50)" data-validation="number" data-validation-optional="false" class="form-control" name="phone" id="phone" value="">
              </fieldset>

		<fieldset class="form-group">
                <label for="photo">Photo</label>
                <div><span style="float:left;"><img alt="" src="#" id="blah"></span></div>
                 <div class="fileUpload" id="image">
                 <i style="font-size: 18px;margin-right: 8px;" class="icon-upload-alt"></i><span>Upload</span>
                 <input type="file" name="photo" id="photo" onchange="readURL(this);" class="upload" accept="image/*"/>
                </div>
              </fieldset>

              <fieldset class="form-group">
              <button type="submit" class="btn btn-primary">Registration</button>
              </fieldset>
    </form>
    <input type="hidden" id="baseUrl" value="<?php echo base_url();?>"/>
    </div><!-- regform -->
<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url();?>assets/js/form-validator/jquery.form-validator.js"></script>
<script src="<?php echo base_url();?>assets/js/form-validator/security.js"></script>
<script>
// just for the demos, avoids form submit

/*$( "#myform" ).validate({
  rules: {
    password: "required",
    repassword: {
      equalTo: "#password"
    }
  }
});*/

	

$.validate({
	  modules : 'security',
	  onError : function() {
			 $(":input.error:first").focus();
			 return false;
		    },
    onValidate : function() {
		 errortext	=	$("#email").attr('current-error');
		 if(errortext!='') {
			  return {
		        element : $('#email'),
		        message : errortext,
		      }
		 }
		 
	    },
	});
	
function readURL(input) {
	var ext = $('#photo').val().split('.').pop().toLowerCase();
	if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
		$('#photo').val('');
		 $('#blah').attr('src', '');
// 		$("#photo" ).attr( "current-error", "Invalid File Format.Allows Only Image File!" );
// 		$('#photo').removeClass('valid').addClass('error');
// 		$("#image").removeClass('has-success').addClass('has-error');
// 		$('#photo').html('<p class="msg error-msg">Invalid File Format.Allows Only Image File</p>');
// 		$('.msg .error-msg').show();
	   	alert('Invalid File Format.Allows Only Image File ');
	    return false;
	}
	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        	$('#blah')
            .attr('src', e.target.result)
            .width(100)
            .height(100);
        }
        reader.readAsDataURL(input.files[0]);
    } 
}
   
</script>
    
 </div>
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->
