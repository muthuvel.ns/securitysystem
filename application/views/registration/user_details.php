<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="col-sm-12 disp-details" id="devicebtn">
	          <span class="titlehead">Supplier Details</span>
          		<a href="<?php echo base_url().'index.php/registration/'?>" class="btn btn-primary">Add Supplier</a>
        </div>
         <div class="col-sm-12">
        <?php if (!empty($message)) : ?>
					<script>
						$(function () {
							 $('#success_msg').slideDown();
								setTimeout(function() {
						 			 $('#success_msg').slideUp();
								}, 3000);
						});
					</script>
					<div class="alert_wrapper" id="success_msg" style="display:block;">
						<div class="alert alert-large alert-success"><button class="close" data-dismiss="alert"></button><center><?php echo $message; ?></center></div>
						<a href="javascript:;"  onclick="$('.sucessfull-message').hide();"></a>
					</div>
				<?php endif; ?>
		</div>
        <div class="col-sm-12">
        <?php if(!empty( $user )){?>
         <table class="table" id="exampletable">
            <thead >
              <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>DeviceID</th>
                <th><i class="icon-edit"></i> <i class="icon-trash"></i></th>
              </tr>
            </thead>
            <tbody>
            <?php
            	$i=1;
            	foreach ( $user as $key => $value ) { ?>
            	<tr>
                <td><?php echo  $i;?></td>
                <td><a href="<?php echo base_url().'index.php/profile/index?uid='.$value['userGuid']?>"><?php echo  (!empty( $value['username'] ) ? $value['username']: '');?></a></td>
                <td><?php echo  (!empty( $value['email'] ) ? $value['email']: '');?></td>
                <td><?php echo  (!empty( $value['phone'] ) ? $value['phone']: '');?></a></td>
                <td><?php echo  (!empty( $value['device_id'] ) ? $value['device_id']: '');?></td>
                <td><a href="<?php echo base_url().'index.php/tracking/disabledevice?uid='.$value['userGuid'].'&user=user';?>" class="editbtn" id="#delete"> <i class="icon-trash"></a></td>
              </tr>
            		
        <?php 	$i++;}
            ?>
            </tbody>
          </table>
       <?php } else { echo '<center><h4>Details Not Found</h4></center>';}?>
        </div>

      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
	$('#exampletable').DataTable();
});
</script>





