<?php 
	$trackId	=	'';
	if( isset( $_REQUEST['tid'] ) && !empty( $_REQUEST['tid' ] ) ) {
		$trackId	=	$_REQUEST['tid'];
	}
?>

<input type="hidden" id="loc" value=''/>
<input type="hidden" id="location" value='<?php echo (!empty($points)?json_encode($points):''); ?>'/>
<input type="hidden" id="waypoint" value='<?php echo (!empty($waypoints)?json_encode($waypoints):''); ?>'/>
<input type="hidden" id="marker" value='<?php echo (!empty($marker)?json_encode($marker):''); ?>'/>
<input type="hidden" id="baseurl" value='<?php echo base_url(); ?>'/>
<input type="hidden" id="trackId" value='<?php echo $trackId; ?>'/>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       <?php if (!empty($message)) : ?>
					<script>
						$(function () {
							 $('#success_msg').slideDown();
								setTimeout(function() {
						 			 $('#success_msg').slideUp();
								}, 3000);
						});
					</script>
					<div class="alert_wrapper" id="success_msg" style="display:block;">
						<div class="alert alert-large alert-success"><button class="close" data-dismiss="alert"></button><center><?php echo $message; ?></center></div>
						<a href="javascript:;"  onclick="$('.sucessfull-message').hide();"></a>
					</div>
				<?php endif;?>
        <div class="col-md-12">
         <div id="map"></div>
        </div>

      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&libraries=places&callback=initMap"
        async defer></script>  -->
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
            <script>

            var map;
            var waypoints;
            var markerpoint;
            var locations1;
            
            function initialize() {
			     
	             if($('#location').val()!=''){
	            	 locations1   = JSON.parse( $('#location').val());
	             }
                if($('#waypoint').val()!=''){
                  waypoints    = JSON.parse($('#waypoint').val());
                }
                if($('#marker').val()!=''){
                  markerpoint    =   JSON.parse($('#marker').val());
                }
                         
              directionsDisplay = new google.maps.DirectionsRenderer({
                    suppressMarkers: true
            //          polylineOptions: polylineOptionsActual
              });
              var bangalore = { lat: 11.652236, lng: 78.793945 };
              var mapOptions = {
                zoom:8,
                center: bangalore
              };
              map = new google.maps.Map(document.getElementById('map'), mapOptions);
              directionsDisplay.setMap(map);
              
        var infoWindow = new google.maps.InfoWindow();
        
                var lat_lng = new Array();
                var latlngbounds = new google.maps.LatLngBounds();
         var baseurl  =   $('#baseurl').val();
                //Initialize the Path Array
             var path = new google.maps.MVCArray();
         
              var i=0;
                directionsServices = [];
                directionsDisplays = [];
                if(locations1!='' && locations1!=undefined){
	                $.each(locations1, function(key, location){
	                  directionsServices[i] = new google.maps.DirectionsService();
	                  var start = new google.maps.LatLng(location.start_lat, location.start_lng);
	                  var end = new google.maps.LatLng(location.end_lat, location.end_lng);
	                  var waypts = [];
	                  var path1 = [];
		               if(waypoints!='' && waypoints!=undefined){
		                  $.each(waypoints, function(way, value){
		                      if(way==location.tracking_id){
		                          $.each(value, function(key, point){
		                            if (point.lat!=undefined && point.lng!= undefined ){
		                            	var color	=	"yellow";
		                            	var colorWeight	=	3;
		                            	if (i % 2 == 0){
		                            		color	=	"green";
		                            		colorWeight	=	9;
		                            	}
		                                
			                                /** set waypoints  **/
			                                  stop = new google.maps.LatLng(point.lat, point.lng);
			                                  waypts.push({
			                                              location:stop,
			                                              stopover:true
			                                              });
                                              /** set marker point */
                                              
                                              if(markerpoint!='' && markerpoint!=undefined){
				                                  $.each(markerpoint, function(mar, marValue){
					                                    if(mar==location.tracking_id){
					                                    	//var iconPath= baseurl+'assets/ico/map'+i+'.png';
					                                        //console.log(baseurl+'assets/ico/map'+i+'.png');
					                                          $.each(marValue, function(key1, point1){  
					                                        	  
					                                        	  var myLatlng = new google.maps.LatLng(point1.lat, point1.lng);
					                                        	  lat_lng.push(myLatlng);
					                                        	  var geocoder	=   new google.maps.Geocoder();
					                                          	geocoder.geocode({
					                                                  'latLng': myLatlng
					                                              }, function(place, status) {
// 					                                        		    if (status === google.maps.GeocoderStatus.OK) {
																			var address = "unknown "+ myLatlng;
																			if(status === google.maps.GeocoderStatus.OK ){
																				address	=	 place[0].formatted_address;
																			}
																			
					                                        		      var marker = new google.maps.Marker({
					                                        		    	  position: myLatlng,
								                                              map: map,
								                                              icon: point1.image,
// 								                                              label: point1.name,
								                                              title: mar
					                                        		      });
					                                        		      
					                                        		      latlngbounds.extend(marker.position);
							                                                (function (marker, point1) {
							                                                    google.maps.event.addListener(marker, "click", function (e) {
							                                                    	 var text	= 'TrackingID:'+mar+'<br>Name: '+point1.username+'<br> Mobile: '+point1.mobile+'<br><div><strong> Place: ' +address; + '</strong><br>';
							                                                        infoWindow.setContent(text);
							                                                        infoWindow.open(map, marker);
							                                                    });
							                                                })(marker, point1);
							                                                map.setCenter(latlngbounds.getCenter());
							                                                map.fitBounds(latlngbounds);
							                                                
// 					                                        		    }
					                                        		  });

						                                          
					                                            /** set marker icon and title  **/
					                                            /*    var myLatlng = new google.maps.LatLng(point1.lat, point1.lng);
					                                                var address = findlocation( point1.lat, point1.lng );
					                                                console.log(address);
					                                                lat_lng.push(myLatlng);
					                                                var marker = new google.maps.Marker({
												                                                position: myLatlng,
												                                                map: map,
												                                                icon: point1.image,
												                                                title: mar
								                                              		  });
				                                              		  
					                                                latlngbounds.extend(marker.position);
					                                                (function (marker, point1) {
					                                                    google.maps.event.addListener(marker, "click", function (e) {
					                                                    	 var text	= 'Name: '+point1.username+'<br> Mobile: '+point1.mobile+'<br>Full address is: ' + marker.position;
					                                                        infoWindow.setContent(text);
					                                                        infoWindow.open(map, marker);
					                                                    });
					                                                })(marker, point1);
					                                                map.setCenter(latlngbounds.getCenter());
					                                                map.fitBounds(latlngbounds);*/
					                                        });
					                                     }
					                                 });
                                              }
				                                    /** set poly line  **/
				                                    path1.push(new google.maps.LatLng(point.lat, point.lng));
				                                    var poly = new google.maps.Polyline({ path:path1, map: map,strokeColor: color,strokeOpacity: 1.0,strokeWeight: colorWeight});
		                                }
		                            });
		
		                          }
		
		                      });
		                  }
	
	                  var request = {
	                              origin: start,
	                              destination: end,
	                              waypoints: [],
	                              optimizeWaypoints: true,
	                              travelMode: google.maps.TravelMode.DRIVING
	                          };
	                directionsServices[i].route(request, function(response, status) {
	                    if (status == google.maps.DirectionsStatus.OK) {
	                        directionsDisplays.push(new google.maps.DirectionsRenderer({preserveViewport:true}));
	                      directionsDisplays[directionsDisplays.length-1].setMap(map);
	                      directionsDisplays[directionsDisplays.length-1].setDirections(response);
	                    } else alert("Directions request failed:"+status);
	                 });
	                i++;
	                });
	             }
                
               }

	google.maps.event.addDomListener(window, 'load', initialize);
           
var i=1;
var baseurl = $('#baseurl').val();
setInterval(function() {
/* var req= $.ajax({
          type:"post",
          url:baseurl+"index.php/tracking/ajaxcall?ajax="+i,
          dataType: 'json',
          success:function(result)
          {
      //         localpoint();
               i++;
          }
        }); */

localpoint();
    //call $.ajax here
}, 1000*60*1);

function localpoint(){
	var trackId = $('#trackId').val();
	var trackUrl = '';
	if(trackId!='' && trackId!= null){
		trackUrl	=	'&tid='+trackId;
	}
     $.ajax({
         type:"post",
         url:baseurl+"index.php/tracking/index?ajax=1"+trackUrl,
         dataType: 'json',
         success:function(result) {
        	 var location = $('#location').val();
        	 var flag =1;
        	 if(location == '0' || location == ''){flag =0;}
        	 
	         $.each(result, function(key, value){
	             if(key=='points'){
	                $('#location').val(JSON.stringify(value));
	             }
	             if(key=='waypoints'){
	              $('#waypoint').val(JSON.stringify(value));
	             }
	             if(key=='marker'){
	                  $('#marker').val(JSON.stringify(value));
	              }
	        });
		        
	         var location1 = $('#location').val();
	         if(location1 == '0' || location1 == ''){ flag =0;}
	         
//		        if(flag==1) {
	         		initialize();
//		        }
         }
       });
}

</script>
