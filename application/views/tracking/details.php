<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="col-sm-12">
	          <h2 class="notifyhead">Tracking Details</h2>
		          <?php if (!empty($message)) : ?>
					<script>
						$(function () {
							 $('#success_msg').slideDown();
								setTimeout(function() {
						 			 $('#success_msg').slideUp();
								}, 3000);
						});
					</script>
					<div class="alert_wrapper" id="success_msg" style="display:block;">
						<div class="alert alert-large alert-success"><button class="close" data-dismiss="alert"></button><?php echo $message; ?></div>
						<a href="javascript:;"  onclick="$('.sucessfull-message').hide();"></a>
					</div>
				<?php endif; ?>
			</div>
        </div>
        <div class="col-sm-12">
        <?php if(!empty( $user )){?>
         <table class="table" id="exampletable">
            <thead >
              <tr>
                <th>S.No</th>
                <th>Device Name</th>
                <th>Source</th>
                <th>Destination</th>
                <th>User Name</th>
                <th><i class="icon-trash"></th>
              </tr>
            </thead>
            <tbody>
            <?php
            	$i=1;
            	foreach ( $user as $key => $value ) { ?>
            	<tr>
                <td><?php echo  $i;?></td>
                <td><?php echo  (!empty( $value['device_name'] ) ? $value['device_name']: 'Mobile Device');?></td>
                <td><?php echo  (!empty( $value['source'] ) ? $value['source']: 'xxxxxx');?></td>
                <td><?php echo  (!empty( $value['destination'] ) ? $value['destination']: 'xxxxxx');?></td>
                <td title="User Map View"><a href="<?php echo base_url().'index.php/tracking/index?uid='.$value['userGuid']?>"><?php echo  (!empty( $value['user_name'] ) ? $value['user_name']: 'xxxxxx');?></a></td>
                <td><a href="<?php echo base_url().'index.php/tracking/disabledevice?uid='.$value['userGuid'];?>" class="editbtn" id="#delete"> <i class="icon-trash"></a></td>
              </tr>
            		
        <?php 	$i++;}
            ?>
            </tbody>
          </table>
       <?php } else { echo '<center><h4>Details Not Found</h4></center>';}?>
        </div>

      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
	$('#exampletable').DataTable();
});
</script>





