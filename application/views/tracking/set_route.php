<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
		<?php if (!empty($message)) : ?>
					<script>
						$(function () {
							 $('#success_msg').slideDown();
								setTimeout(function() {
						 			 $('#success_msg').slideUp();
								}, 3000);
						});
					</script>
					<div class="alert_wrapper" id="success_msg" style="display:block;">
						<div class="alert alert-large alert-success"><button class="close" data-dismiss="alert"></button><center><?php echo $message; ?></center></div>
						<a href="javascript:;"  onclick="$('.sucessfull-message').hide();"></a>
					</div>
				<?php endif;?>
        <div class="col-md-8">
           <div id="map"></div>
        </div>
        <div class="col-md-4" >
          <form action="<?php echo base_url().'index.php/tracking/insertlocation';?>" method="post" id="routeform">
          	<input type="hidden" id="lat" name="start_lat" value=""/>
			<input type="hidden" id="lng" name="start_lng" value=""/>
			<input type="hidden" id="lat1" name="end_lat" value=""/>
			<input type="hidden" id="lng1" name="end_lng" value=""/>
			<input type="hidden" id="source" name="source" value=""/>
			<input type="hidden" id="destination" name="destination" value=""/>
			
            <div><label>Device ID</label></br>
            <?php echo form_dropdown('device_id', $device, '', 'class="form-control" required'); ?>
            <div></br>
            <label>User Name</label></br>
            <?php echo form_dropdown('user_id', $user, '', 'class="form-control" required'); ?></br>
            <div>
            <div class="row">
			  <div class="col-md-6"><label>Hours</label> <input type="number" maxlength="3" required="" id="hour" class="form-control" name ="hour"></div>
			  <div class="col-md-6"><label>Min</label><input type="number" maxlength="2" required="" id="min" class="form-control" name ="min"></div>
			</div></br>
            <input type="submit" id="submit" value="Submit" name="submit" class="submit" ></div>
          </form>
        </div>

      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->
<input id="origin-input" class="controls orgin" type="text" placeholder="Enter an origin location">
<input id="destination-input" class="controls dest" type="text" placeholder="Enter a destination location">
			
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
 <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap" async defer></script>
<script>
function initMap() {
	  var origin_place_id = null;
	  var destination_place_id = null;
	  var markersArray	=	[];
	  var markersArray1	=	[];
	  var travel_mode = google.maps.TravelMode.WALKING;
	  var map = new google.maps.Map(document.getElementById('map'), {
	    mapTypeControl: false,
	    center: {lat: 11.652236, lng: 78.793945},
	    zoom: 8
	  });
	  var directionsService = new google.maps.DirectionsService;
	  var directionsDisplay = new google.maps.DirectionsRenderer;
	 // directionsDisplay.setMap(map);

	  var origin_input = document.getElementById('origin-input');
	  var destination_input = document.getElementById('destination-input');
//	  var modes = document.getElementById('mode-selector');

	  map.controls[google.maps.ControlPosition.TOP_LEFT].push(origin_input);
	  map.controls[google.maps.ControlPosition.TOP_LEFT].push(destination_input);
//	  map.controls[google.maps.ControlPosition.TOP_LEFT].push(modes);

	  travel_mode = google.maps.TravelMode.DRIVING;
	  var origin_autocomplete = new google.maps.places.Autocomplete(origin_input);
	  origin_autocomplete.bindTo('bounds', map);
	  var destination_autocomplete =  new google.maps.places.Autocomplete(destination_input);
	  destination_autocomplete.bindTo('bounds', map);

	  // Sets a listener on a radio button to change the filter type on Places
	  // Autocomplete.
	  /* function setupClickListener(id, mode) {
	    var radioButton = document.getElementById(id);
	    radioButton.addEventListener('click', function() {
	      travel_mode = mode;
	    });
	  } */
//	  setupClickListener('changemode-walking', google.maps.TravelMode.WALKING);
//	  setupClickListener('changemode-transit', google.maps.TravelMode.TRANSIT);
//	  setupClickListener('changemode-driving', google.maps.TravelMode.DRIVING);

	  function expandViewportToFitPlace(map, place) {
	    if (place.geometry.viewport) {
	      map.fitBounds(place.geometry.viewport);
	    } else {
	      map.setCenter(place.geometry.location);
	      map.setZoom(7);
	    }
	  }

	  origin_autocomplete.addListener('place_changed', function() {
	    var place = origin_autocomplete.getPlace();
	    if (!place.geometry) {
	      window.alert("Autocomplete's returned place contains no geometry");
	      return;
	    }
	    expandViewportToFitPlace(map, place);

	    // If the place has a geometry, store its place ID and route if we have
	    // the other place ID
		document.getElementById("lat").value =place.geometry.location.lat();
	    origin_place_id = place.geometry.location;
	    
	    document.getElementById("lat").value = place.geometry.location.lat();
	    document.getElementById("lng").value = place.geometry.location.lng();
	   
	   route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay);
	  });

	  destination_autocomplete.addListener('place_changed', function() {
		 
	    var place = destination_autocomplete.getPlace();
	    if (!place.geometry) {
	      window.alert("Autocomplete's returned place contains no geometry");
	      return;
	    }
	    expandViewportToFitPlace(map, place);

	    // If the place has a geometry, store its place ID and route if we have
	    // the other place ID
	    destination_place_id = place.geometry.location;
		
	    document.getElementById("lat1").value = place.geometry.location.lat();
	    document.getElementById("lng1").value = place.geometry.location.lng();

	    route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay);
	  });

	  function route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay) {
	   /*  if (!origin_place_id || !destination_place_id) {
	      return;
	    } */

	    deleteOverlays();
	    
	    var marker = new google.maps.Marker({
		    map: map,
		    draggable: true,
		    title:'start point',
		    label: 'A',
		    position: origin_place_id
		  });

	 // add marker in markers array
        markersArray.push(marker);

        deleteOverlays1();
		var marker1 = new google.maps.Marker({
		    map: map,
		    draggable: true,
		    title:'End point',
		    label: 'B',
		    position: destination_place_id
		  });
		// add marker in markers array
        markersArray1.push(marker1);
		
		google.maps.event.addListener(marker, 'dragend', function (event) {
		    document.getElementById("lat").value = this.getPosition().lat();
		    document.getElementById("lng").value = this.getPosition().lng();

		    var myLatlng = new google.maps.LatLng(this.getPosition().lat(), this.getPosition().lng());
		    var geocoder	=   new google.maps.Geocoder();
         	geocoder.geocode({
                 'latLng': myLatlng
             }, function(place, status) {
//        		    if (status === google.maps.GeocoderStatus.OK) {
							var address = "unknown "+ myLatlng;
							if(status === google.maps.GeocoderStatus.OK ){
								address	=	 place[0].formatted_address;
							}
							$('#origin-input').val(address);
       		  });
		});

		google.maps.event.addListener(marker1, 'dragend', function (event) {
		    document.getElementById("lat1").value = this.getPosition().lat();
		    document.getElementById("lng1").value = this.getPosition().lng();

		    var myLatlng = new google.maps.LatLng(this.getPosition().lat(), this.getPosition().lng());
		    var geocoder	=   new google.maps.Geocoder();
         	geocoder.geocode({
                 'latLng': myLatlng
             }, function(place, status) {
						var address = "unknown "+ myLatlng;
						if(status === google.maps.GeocoderStatus.OK ){
							address	=	 place[0].formatted_address;
						}
						$('#destination-input').val(address);
       		  });
		});

		function deleteOverlays() {
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);
                }
            markersArray.length = 0;
            }
        }

		function deleteOverlays1() {
            if (markersArray1) {
                for (i in markersArray1) {
                    markersArray1[i].setMap(null);
                }
            markersArray1.length = 0;
            }
        }
	   /* directionsService.route({
	      origin: origin_place_id,
	      destination: destination_place_id,
	      travelMode: travel_mode
	    }, function(response, status) {
	      if (status === google.maps.DirectionsStatus.OK) {
	        directionsDisplay.setDirections(response);
	      } else {
	        window.alert('Directions request failed due to ' + status);
	      }
	    });*/
	  }
	}
//   google.maps.event.addDomListener(window, 'load', initMap);

$('#submit').click(function(){

	var orgin 		= $('#origin-input').val();
	var destination	= $('#destination-input').val();

	var flag = 0;

	if(orgin==null || orgin =='' ) {
		flag = 1;
	}

	if(destination==null || destination =='' ) {
		flag = 1;
	}

	if(flag==1){
		alert('Please fill soure and destination');
		return false;
	}

	var source 		= $('#source').val(orgin);
	var dest		= $('#destination').val(destination);
});
</script>