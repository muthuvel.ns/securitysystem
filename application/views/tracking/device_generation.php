<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="col-sm-12" id="devicebtn">
          <span class="titlehead">Device Details</span>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add" data-whatever="@mdo">Add Device</button>
        </div>
        </br>
        </br>
        <div class="col-sm-12">
	        <?php if (!empty($message)) : ?>
				<script>
					$(function () {
						 $('#success_msg').slideDown();
							setTimeout(function() {
					 			 $('#success_msg').slideUp();
							}, 3000);
					});
				</script>
				<div class="alert_wrapper" id="success_msg" style="display:block;">
					<div class="alert alert-large alert-success"><button class="close" data-dismiss="alert"></button><center><?php echo $message; ?></center></div>
					<a href="javascript:;"  onclick="$('.sucessfull-message').hide();"></a>
				</div>
			<?php endif; ?>
        </div>
        <div class="col-sm-12">
         <table class="table" id="exampletable">
            <thead class="thead-default">
              <tr>
                <th>S.No</th>
                <th>Device ID</th>
                <th>Device Name</th>
                <th><i class="icon-edit"></i> <i class="icon-trash"></i></th>
              </tr>
            </thead>
            <tbody>
            <?php if(!empty( $deviceDetails ) ) { 
            	$i=1;
	            foreach ( $deviceDetails as $key=>$value){ ?>
		            <tr>
	                <td><?php echo $i;?></td>
	                <td><?php echo  $value['device_id']?></td>
	                <td><?php echo  $value['name']?></td>
	                <td><button type="button" class="editbtn" id="#edit_<?php echo  $value['device_id']?>"  onclick="edit('<?php echo  $value['name']?>', '<?php echo  $value['device_id']?>')" > <i class="icon-edit"></i></button>
	                <a href="<?php echo base_url().'index.php/tracking/disabledevice?dev_id='.$value['device_id'];?>" class="editbtn"> <i class="icon-trash"></i></a>
	                </td>
	              </tr>
	            	
	     <?php  $i++;}
            }else {echo '<center><h4>Details Not Found</h4></center>';}?>
              
            </tbody>
          </table>
        </div>

        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="add_title">Device Details</h4>
              </div>
              <form action="<?php echo base_url().'index.php/tracking/generatedevice?type=add'; ?>" method="post">
	              <div class="modal-body">
	                  <div class="form-group">
	                    <label for="device_id" class="control-label">Device ID : </label>
	                    <input type="text" class="form-control" id="device_id" name="device_id" value="" required="">
	                  </div>
	                  <div class="form-group">
	                    <label for="device_name" class="control-label">Device Name </label>
	                    <input type="text" class="form-control" id="device_name" name="device_name" value="" required="">
	                  </div>
	              </div>
	              <div class="modal-footer">
	                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	                <input type="submit" class="btn btn-primary" value="Submit"/>
	              </div>
              </form>
            </div>
          </div>
        </div>

        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="edit_title">Device Details</h4>
              </div>
              <form action="<?php echo base_url().'index.php/tracking/generatedevice?type=up'; ?>" method="post">
	              <div class="modal-body">
	               <div class="form-group">
                    <label for="recipient-name" class="control-label">Device ID : </label>
                    <input type="text" class="form-control" id="edit_device_id" name="device_id" value="" readonly>
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="control-label">Device Name </label>
                    <input type="text" class="form-control" id="edit_device_name" name="device_name" value="">
                  </div>
	              </div>
	              
	              <div class="modal-footer">
	                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	                 <input type="submit" class="btn btn-primary" value="Save Changes"/>
	              </div>
               </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
	$('#exampletable').DataTable();
} );
	
$('#add').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
//  var modal = $(this)
 // modal.find('.modal-title').text('Device Details ' + recipient)
 // modal.find('.modal-title').text('Device Details ' )
//  modal.find('.modal-body input').val(recipient)
})

function edit(name, deviceId){
	$('#edit').modal('show');
	var modal = $("#edit");
	 $(".modal-body #edit_device_id").val( deviceId );
	 $(".modal-body #edit_device_name").val( name );
	 $('#edit_title').html('Edit Device Details');
	
}
/*$('#edit').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
 // modal.find('.modal-title').text('Device Details ' + recipient)
  modal.find('.modal-title').text('Device Details ' )
  modal.find('.modal-body input').val(recipient)
})
*/
</script>
