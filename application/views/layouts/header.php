<?php 
$userRoleGuid	=	(!empty( $this->session->userdata['roleguid'] ) ? $this->session->userdata['roleguid']:'');
$userGuid		=	(!empty( $this->session->userdata['userguid'] ) ? $this->session->userdata['userguid']:'');

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js" rel="stylesheet">
<!--link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet"-->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/pages/dashboard.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script> 
<link href="<?php echo base_url();?>assets/css/pages/menu.css" rel="stylesheet">


<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- Notification -->

 <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/notification_menu/css/style_light.css">

    <script src="<?php echo base_url();?>assets/notification_menu/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/notification_menu/js/ttw-notification-menu.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/notification_menu/demo.js" type="text/javascript"></script>
-->

<!-- Notification End -->


</head>
<body>

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">

    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="<?php echo base_url();?>index.php/tracking"><img src="<?php echo base_url();?>assets/img/196X40_2.png"> </a>
      <div class="nav-collapse">
<!--ul class="ttw-notification-menu">

    <li id="tasks" class="notification-menu-item"><a href="#">Notification</a></li>
<!--span title="Notifications" class="notification-bubble" style="background-color: rgb(254, 193, 81); display: inline;">0</span-->
<!--/ul-->
  <div class="spinner-master">
    <input type="checkbox" id="spinner-form" />
    <label for="spinner-form" class="spinner-spin">
    <div class="spinner diagonal part-1"></div>
    <div class="spinner horizontal"></div>
    <div class="spinner diagonal part-2"></div>
    </label>
  </div>
        <ul class="nav pull-right">
          <!--li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li-->


          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
           <i class="icon-cog"></i> </a>
           
            <ul class="dropdown-menu">
              <!--li><a href="javascript:;">Profile</a></li-->
              <li><a href="<?php echo base_url().'index.php/logout';?>">Logout</a></li>
              <?php if ( $userRoleGuid == ADMIN_ROLE_ID ) { // superadmin only have permission on add admin details?>
        		<li><a href="<?php echo base_url().'index.php/profile/index?uid='.$userGuid.'&rid='.$userRoleGuid;?>"> Profile</a> </li>
       		 <?php } ?>
            </ul>
          </li>
        </ul>
        <!--form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form-->
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">

  <nav id="menu" class="menu">
      <ul class="mainnav" >
        <li class="active"><a href="<?php echo base_url();?>index.php/tracking"><div class="text-center" id="mobhome" ><i class="icon-home" id="menuicons"></i></br><span>Security System</span></div></a> </li>
        <li><a href="<?php echo base_url();?>index.php/tracking/generatedevice"><div class="text-center"><i class="icon-cogs" id="menuicons"></i></br><span>Device Details</span></div> </a> </li>
        <li><a href="<?php echo base_url();?>index.php/registration/userdetails"><div class="text-center"><i class="icon-group" id="menuicons"></i></br><span> Supplier Details</span></div> </a> </li>
        <li><a href="<?php echo base_url();?>index.php/tracking/details"><div class="text-center"><i class="icon-list-alt" id="menuicons"></i></br><span>Tracking Details</span></div> </a></li>
        <!--li><a href=""><div class="text-center"><i class="icon-code" id="menuicons"></i></br><span>Tracking Map</span></div> </a> </li-->
        <li><a href="<?php echo base_url();?>index.php/tracking/setroute"><div class="text-center"><i class="icon-map-marker" id="menuicons"></i></br><span>Set Routes</span></div> </a> </li>
        <?php if ( $this->session->userdata['roleguid'] == SUPER_ADMIN_ROLE_ID ) { // superadmin only have permission on add admin details?>
        <li><a href="<?php echo base_url().'index.php/registration/admindetails/'.ADMIN_ROLE_ID;?>"><div class="text-center"><i class="icon-user" id="menuicons"></i></br><span>Admin Details</span></div> </a> </li>
        <?php } ?>
        <li><a href="<?php echo base_url();?>index.php/notified"><div class="text-center"><i class="icon-bell-alt" id="menuicons"></i></br><span>Notified Details</span></div> </a> </li>  


        <!--li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><div class="text-center"><i class="icon-long-arrow-down" id="menuicons"></i></br><span>Drops</span></div> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="">Icons</a></li>
            <li><a href="">FAQ</a></li>
            <li><a href="">Pricing Plans</a></li>
            <li><a href="">Login</a></li>
            <li><a href="">Signup</a></li>
            <li><a href="">404</a></li>
          </ul>
        </li-->
      </ul>
</nav>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>




<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/js/base.js"></script> 

<script>

 $(function(){
          //Keep track of last scroll
          var lastScroll = 0;
          $(window).scroll(function(event){
              //Sets the current scroll position
              var st = $(this).scrollTop();

              //Determines up-or-down scrolling
              if (st > lastScroll){
                $(".footer").css("display",'inline')
              } 
              if(st == 0){
                $(".footer").css("display",'none')
              }
              //Updates scroll position
              lastScroll = st;
          });
        });

var $menu = $('#menu'), $menulink = $('#spinner-form'), $search = $('#search');
$menulink.click(function (e) {
    $menulink.toggleClass('active');
    $menu.toggleClass('active');
    if ($search.hasClass('active')) {
        $('.menu.active').css('padding-top', '50px');
    }
});

</script>
