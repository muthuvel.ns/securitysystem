<style>
.table{
 /*   background: #04c3df none repeat scroll 0 0;
    color: floralwhite; */
   border: 1px solid #faf0e6;
    border-radius: 4px;
    box-shadow: 2px 2px 2px #faf0e6;
    font-size: 16px;
    margin-bottom: 20px;
    max-width: 100%;
    width: 100% !important;
}

.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    border-top: medium none;
    word-wrap: break-word;
}
td {
    word-break: break-all;
}

th {
    width: 35%;
}

</style>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="col-md-12">
       <?php if (!empty( $user ) ) {
       		$imageUrl	=	(!empty( $user[0]['photo'] ) ? 'assets/upload_images/'.$user[0]['photo'] :'assets/img/profile.png');
       		$username	=	(!empty( $user[0]['username'] ) ? $user[0]['username'] :'');
       		$email		=	(!empty( $user[0]['email'] ) ? $user[0]['email'] :'');
       		$phone		=	(!empty( $user[0]['phone'] ) ? $user[0]['phone'] :'');
       		$deviceId	=	(!empty( $user[0]['device_id'] ) ? $user[0]['device_id'] :'');
       	?> 
          <div class="devicenotify">
            <span style="float:left;"><img src="<?php echo base_url().$imageUrl;?>" width="125" height="125" class="devicenotifyimg"></span>
            <div class="details">
            <table>
             	<tr><td>Address</td><td>&nbsp;:&nbsp;</td><td class="textword" title="<?php echo $username;?>"><?php echo $username;?></td></tr>
                <tr><td>Email</td><td>&nbsp;:&nbsp;</td><td class="textword"title="<?php echo $email;?>"><span style="text-decoration:underline;color:#3278BC;"><?php echo $email;?> </span></td></tr>
                <tr><td>Phone Numer</td><td>&nbsp;:&nbsp;</td><td  class="textword" title="<?php echo $phone;?>"><?php echo $phone;?></td></tr>
                <tr><td>Device ID</td><td>&nbsp;:&nbsp;</td><td class="textword" title="<?php echo $deviceId;?>"><?php echo $deviceId;?></td></tr>
              </table>
            </div>
             <?php if ( !empty( $notification ) ) {
            	
            	foreach ( $notification as $key => $value ) {
	            	$message	=	(!empty( $value['activity_data1'] ) ? $value['activity_data1'] :'');
	            	$mobile		=	(!empty( $value['activity_data2'] ) ? $value['activity_data2'] :'');
	            	$date		=	(!empty( $value['client_date'] ) ? $value['client_date'] :'');
	            	$deviceId	=	(!empty( $value['device_id'] ) ? $value['device_id'] :'');
	            	$source		=	'';
	            	$destination		=	'';
	            	$userData	=	(!empty( $value['activity_data3'] ) ? json_decode( $value['activity_data3'], true ) :'');
	            	if ( !empty( $userData['user'] )) {
	            		foreach ( $userData['user'] as $data){
	            			$source		=	(!empty( $data['source'] ) ? $data['source'] :'');
	            			$destination=	(!empty( $data['destination'] ) ? $data['destination'] :'');
	            		}
	            			
            		}
            	?>
            <div class="col-md-12 ">

              <table class="table">
                <tr>
                  <th>Source</th><td><?php echo $source;?></td>
                </tr>

                <tr>
                  <th>Destination</th><td><?php echo $destination;?></td>
                </tr>
                
                <tr>
                  <th>Owner Number</th><td><?php echo $mobile;?></td>
                </tr>
                <tr>
                  <th>Notification Message</th><td><?php echo $message; ?></td>
                </tr>

                <tr>
                  <th>Device ID</th><td><?php echo $deviceId; ?></td>
                </tr>

                <tr>
                  <th>Date</th><td><?php echo $date; ?></td>
                </tr>
              </table>
            </div>
            <?php } 
             }else{ echo '<div class="col-md-12 "> <h4>Notification Details not found</h4></div>';}?>
          </div>
          <?php }else{ echo '<h4>User Details not found</h4>';} ?>
        </div>

      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


