<?php
Class Tracking_model extends CI_Model
{
function __construct()
    {
        parent::__construct();
    }
/**
 * 
 * Enter description here ...
 * @param $track_id
 */
 function trackingPoints($track_id=0, $mail=0)
 {
   $this -> db -> select('tracking_id, source, destination,start_lat,start_lng,end_lat,end_lng,start_date,end_date,mail');
   
   if(!empty( $track_id )){
   	  $this -> db ->where_in('tracking_id',$track_id);
   }
   
   if(!empty( $mail )){
   	$this -> db ->where('mail',$mail);
   }
   
   $this -> db ->where_in('deleted', 0);
   
   $query = $this -> db -> get('tracking_points');
  
     return $query->result_array();
 }
 
 /**
  * 
  * Enter description here ...
  * @param $track_id
  */
 function wayPoints($track_id= 0, $marker='', $mail=0){
 	$this -> db -> select('id,tracking_id,lat,lng,start_date,end_date,status');
	 if(!empty( $track_id )){
	      $this -> db ->where_in('tracking_id',$track_id);
	   }
	   
    if(!empty( $marker )){
          $this -> db ->where('status',$marker);
       }
       
    if(!empty( $mail )){
          $this -> db ->where('mail',$mail);
       }
       
       $this -> db ->order_by('id','asc');
       $this -> db ->where('deleted',0);
   $query = $this -> db -> get('waypoints');
     return $query->result_array();
 }

 /**
  * 
  * Enter description here ...
  * @param unknown_type $requestData
  */
 function insertTrackingPoints($requestData) {
  
 	$result	=	0;
 	if ( empty( $requestData ) ) {
 		return $result;
 	}
 	
    $result = $this->db->insert_batch('tracking_points', $requestData);
     return $result;
 }
 
 function updateTrackingPoints($track_id, $requestData) {
 	$result	=	0;
 	if ( empty( $track_id ) || empty( $requestData ) ) {
 		return $result;
 	}
 
 	$this->db->where('tracking_id',$track_id);
 	$result = $this->db->update('tracking_points',$requestData);
 	return $result;
 }
 
 /**
  * 
  * Enter description here ...
  * @param unknown_type $track_id
  * @param unknown_type $requestData
  */
function updateWayPoints($track_id, $requestData) {
	$result	=	0;
	if ( empty( $track_id ) || empty( $requestData ) ) {
		return $result;
	}
  
             $this->db->where('tracking_id',$track_id);
    $result = $this->db->update('waypoints',$requestData);
     return $result; 
 }
 
 /**wayPoints
  * 
  * Enter description here ...
  * @param $requestData
  */
function insertWayPoints($requestData) {
	$result	=	0;
	if ( empty( $requestData ) ) {
		return $result;
	}
  
    $result = $this->db->insert_batch('waypoints',$requestData);
     return $result;
 }
 
 /**
  * Bulk update based on filedname ( id,date and etc not id and date value 0, 1)
  * @param string $filedName
  * @param array $requestData
  */
 function BulkUpadateWayPoints( $filedName, $requestData ) {
 	$result	=	0;
 	if ( empty( $requestData ) || empty( $filedName )) {
 		return $result;
 	}
 
 	$result = $this->db->update_batch('waypoints', $requestData, $filedName );
 	return $result;
 }
 
 /**
  * 
  * Enter description here ...
  * @param $requestData
  */
function insertTrackingInfo($requestData) {
	$result	=	0;
	if ( empty( $requestData ) ) {
		return $result;
	} 
    $result = $this->db->insert('tracking_info', $requestData);
     return $result;
 }
 
 function updateTrackingInfo( $device_id, $requestData ) {
 	$result	=	0;
 	if ( empty( $requestData ) || empty( $device_id ) ) {
 		return $result;
 	}
 	$this->db->where('device_id',$device_id);
 	$result = $this->db->update('tracking_info', $requestData);
 	return $result;
 }
 
 /**
  * 
  * Enter description here ...
  * @param $lat1
  * @param $long1
  * @param $lat2
  * @param $long2
  */
 function calculateStartAndEndPoints($lat1, $long1, $lat2, $long2 ){
    
     $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time1 = $response_a['rows'][0]['elements'][0]['duration']['text'];
    
    $time = gmdate("H:i:s",$response_a['rows'][0]['elements'][0]['duration']['value']);
    return $time1;
 }
 
function deviceInfo($device_id=''){
    $this -> db -> select('device_id,name,password');
     
    if(!empty( $device_id )){
          $this -> db ->where_in('device_id',$device_id);
     }
     $this -> db ->where('deleted', 0);
   $query = $this -> db -> get('tracking_info');
     return $query->result_array();
 }

 
 function getdeviceInfo($whereNot='', $whereIn=''){
       $this -> db -> select('device_id, name, password');
       
       if( $whereNot ){
            $this -> db ->where_not_in('device_id', $whereNot);
       }
       
       if( $whereIn ){
            $this -> db ->where_in('device_id', $whereIn);
       }
     
       $this -> db ->where('deleted', 0);
       $query = $this -> db -> get('tracking_info');
         
      return $query->result_array();
 }
 
 function trackingPointsForMail()
 {
 	$this -> db -> select('tracking_id,start_lat,start_lng,end_lat,end_lng,start_date,end_date,mail');
 	$this -> db ->where('mail',0);
 	$this -> db ->where('deleted', 0);
 	$query = $this -> db -> get('tracking_points');
 	 
 	return $query->result_array();
 }
 
 /**
  *
  * Enter description here ...
  * @param $track_id
  */
 function wayPointsForMail($trackId=""){
 	$this -> db -> select('tracking_id,lat,lng,start_date,end_date');
 	if ( !empty( $trackId ) ){
 		$this -> db ->where_in('tracking_id',$trackId);
 	}
 	$this -> db ->where('mail',0);
 	$this -> db ->where('deleted', 0);
 	$this -> db ->where('status', 1);
 	$query = $this -> db -> get('waypoints');
 	return $query->result_array();
 }
 
 function disableGivenDetails( $tablename, $fieldname, $fieldvalue, $updateData ){
 	$result	=	0;
 	if ( empty( $tablename ) || empty( $fieldname ) || empty( $fieldvalue ) || empty( $updateData )) {
 		return $result;
 	}
 	
 	$this->db->where_in($fieldname, $fieldvalue);
 	$result = $this->db->update($tablename, $updateData);
 	return $result;
 	
 }
 
}
?>