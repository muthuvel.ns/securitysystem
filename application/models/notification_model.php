<?php
Class Notification_model extends CI_Model {
function __construct()
    {
        parent::__construct();
    }
 
function getNotificationDetails( $userGuid="", $deviceId="" ){
	$this -> db -> select('user_guid, device_id, client_date, activity_data1, activity_data2, activity_data3, activity_comment');
	
	if( $userGuid ){
		$this -> db ->where_in('user_guid', $userGuid);
	}
	
	if( $deviceId ){
		$this -> db ->where_in('device_id', $deviceId);
	}
	
	$this->db->order_by('id','desc');
	$this->db->limit(10);
	$query = $this -> db -> get('notification_log');
	return $query->result_array();
}
 
 
}
?>
