<?php
Class Trackingmanager extends CI_Model
{
function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Tracking_model','',TRUE);
        $this->load->model('User','',TRUE);
        $this->load->model('Usermanager','',TRUE);
        $this->load->model('Utils','',TRUE);
    }
function getDeviceInfo( $trackingId ="", $deviceId = "" ){
	
		$data['points']       =   array();
		$data['waypoints']    =   array();
		$data['marker']       =   array();
		
		
	    $trackingPoints   = array();
 	    $waypoints        = array();
 	    $marker           = array();
 	    $trackingGuidList = array();
 	    
 	    if( !empty( $deviceId )) {
		 	   $getdeviceDetails	=	$this->User->userTrackingDetails('', $deviceId);
		 	   
		 	   if ( !empty( $getdeviceDetails )) {
			 	   foreach ( $getdeviceDetails as $data ) { 
			 	  		 $trackingGuidList[] = $data['guid'];
			 	   }
		 	   }
		 	   $trackingId	=	(!empty( $trackingGuidList ) ?$trackingGuidList :'');
 	    }
 	    
 	    if( empty( $trackingId ) ) {
 	    	return $data;
 	    }
 	    
 	    if ( !empty($trackingId )) {
	        $trackingPoints  =   $this->Tracking_model->trackingPoints($trackingId);
	        $waypoints       =   $this->Tracking_model->wayPoints($trackingId);
	        $marker          =   $this->Tracking_model->wayPoints($trackingId,$markerStatus=1);
 	    }

        if ( !empty( $trackingPoints )) {
	        $j=1;
	        foreach ( $trackingPoints as $val) {
	            $data['points'][$j]['tracking_id'] = $val['tracking_id'];
	            $data['points'][$j]['start_lat']   = $val['start_lat'];
	            $data['points'][$j]['start_lng']   = $val['start_lng'];
	            $data['points'][$j]['end_lat']     = $val['end_lat'];
	            $data['points'][$j]['end_lng']     = $val['end_lng'];
	           $j++;
	        }
        }
        
        if (  !empty( $waypoints ) ) {
                $i=0;
                foreach ($waypoints as $value) {
                    if ( !empty( $value['tracking_id'] )) {
                        $data['waypoints'][$value['tracking_id']][$i]['lat']    =  $value['lat']; 
                        $data['waypoints'][$value['tracking_id']][$i]['lng']    =  $value['lng']; 
                        $i++;
                    }
                }
            }
        
        if ( !empty( $marker ) ) {
	        $k=0;
	        foreach ($marker as $value1) {
	        	$userExists = $this->User->userTrackingDetails( '', '', '', $value1['tracking_id']);
	        	if ( !empty( $userExists[0]['device_id'] )) {
	 	        	$userInfo 	= $this->User->userAndProfileDetails( $userExists[0]['user_guid'] );
		        	$deviceInfo  =   $this->Tracking_model->deviceInfo( $userExists[0]['device_id'] );
	        	}
	        	
        		$username	=	(!empty( $userInfo[0]['username'] )? $userInfo[0]['username']:'XXXXXX' );
        		$phone		=	(!empty( $userInfo[0]['phone'] )? $userInfo[0]['phone']:'XXXXXX' );
	        	
	        	$imageUrl	=	base_url().'assets/ico/mobile.png';
// 	        	$name		= 'M';
	        	if ( $deviceInfo ) {
	        		$imageUrl	=	base_url().'assets/ico/car1.png';
// 	        		$name		= 'C';
	        	}
	        	
	            $data['marker'][$value1['tracking_id']][$k]['lat']    =  $value1['lat']; 
	            $data['marker'][$value1['tracking_id']][$k]['lng']    =  $value1['lng']; 
	            $data['marker'][$value1['tracking_id']][$k]['image']  =  $imageUrl;
	           	$data['marker'][$value1['tracking_id']][$k]['username']  =  $username;
	            $data['marker'][$value1['tracking_id']][$k]['mobile']  =  $phone;
// 	            $data['marker'][$value1['tracking_id']][$k]['name']  =  $name;
	            $k++;
	        }
        }
        
	return $data;
}


/**
 * API purpose get over all device information
 * @param string $trackingId
 * @param string $deviceId
 */
function getDeviceDetails( $trackingId ="", $deviceId = "", $userGuid="" ){

	$info       =   array();

	$trackingPoints   = array();
	$waypoints        = array();
	$marker           = array();
	$trackingGuidList = array();

	if( !empty( $deviceId )) {
		$getdeviceDetails	=	$this->User->userTrackingDetails('', $deviceId);
		
		if ( !empty( $getdeviceDetails )) {
			foreach ( $getdeviceDetails as $data ) {
				$trackingGuidList[] = $data['guid'];
			}
		}
		$trackingId	=	(!empty( $trackingGuidList ) ?array_unique($trackingGuidList) :'');
	}elseif( !empty( $userGuid )) {
		$getdeviceDetails	=	$this->User->userTrackingDetails('', '', $userGuid);
		if ( !empty( $getdeviceDetails )) {
			foreach ( $getdeviceDetails as $data ) {
				$trackingGuidList[] = $data['guid'];
			}
		}
		$trackingId	=	(!empty( $trackingGuidList ) ?array_unique($trackingGuidList) :'');
	}

	if( empty( $trackingId ) ) {
		return $info;
	}

		$trackingPoints  =   $this->Tracking_model->trackingPoints($trackingId);

	if ( !empty( $trackingPoints )) {
		foreach ( $trackingPoints as $val) {
			$userExists = $this->User->userTrackingDetails( '', '', '', $val['tracking_id']);
			if ( !empty( $userExists[0]['user_guid'] )) {
				$userGuid = $userExists[0]['user_guid'];
				$deviceId = $userExists[0]['device_id'];
				
				$deviceInfo =   $this->Tracking_model->deviceInfo( $deviceId );
				$userInfo 	=	$this->User->userAndProfileDetails( $userGuid );
				$username	=	(!empty( $userInfo[0]['username'] )? $userInfo[0]['username']:'XXXXXX' );
				$phone		=	(!empty( $userInfo[0]['phone'] )? $userInfo[0]['phone']:'XXXXXX' );
				$handsetname=	(!empty( $userInfo[0]['device_name'] )? $userInfo[0]['device_name']:'mobile device' );
				
// 				$info[$userGuid][$j]['tracking_id']	= $val['tracking_id'];
				$info[$userGuid]['activeuser']['user_guid']  	= $userGuid;
				$info[$userGuid]['activeuser']['username']  	= $username;
				$info[$userGuid]['activeuser']['phone']  		= $phone;
				
				$info[$userGuid]['activeuser']['start_lat']  	= $val['start_lat'];
				$info[$userGuid]['activeuser']['start_lng']  	= $val['start_lng'];
				$info[$userGuid]['activeuser']['end_lat']     	= $val['end_lat'];
				$info[$userGuid]['activeuser']['end_lng']    	= $val['end_lng'];
				$info[$userGuid]['activeuser']['source']		= $val['source'];
				$info[$userGuid]['activeuser']['destination'] 	= $val['destination'];
				$info[$userGuid]['activeuser']['start_date']	= $val['start_date'];
				$info[$userGuid]['activeuser']['end_date']  	= $val['end_date'];
				
				if ( $deviceInfo ) {
					$info[$userGuid]['activeuser']['deviceInfo']['device_id']   	= $deviceId;
					$info[$userGuid]['activeuser']['deviceInfo']['name']  			= (!empty( $deviceInfo[0]['name']) ?$deviceInfo[0]['name'] :'');
					$info[$userGuid]['activeuser']['deviceInfo']['image']   		= base_url().'assets/ico/car1.png';
					$info[$userGuid]['activeuser']['deviceInfo']['tracking_id']  	= $val['tracking_id'];
					$info[$userGuid]['activeuser']['deviceInfo']['trackingpointer'] = $this->Tracking_model->wayPoints($val['tracking_id']);
					$info[$userGuid]['activeuser']['deviceInfo']['currentlocation'] = $this->Tracking_model->wayPoints($val['tracking_id'], 1);
				}else{
					$info[$userGuid]['activeuser']['handsetInfo']['device_id']  		= $deviceId;
					$info[$userGuid]['activeuser']['handsetInfo']['name']  				= $handsetname;
					$info[$userGuid]['activeuser']['handsetInfo']['image']  			= base_url().'assets/ico/mobile.png';
					$info[$userGuid]['activeuser']['handsetInfo']['tracking_id']  		= $val['tracking_id'];
					$info[$userGuid]['activeuser']['handsetInfo']['trackingpointer'] 	= $this->Tracking_model->wayPoints($val['tracking_id']);
					$info[$userGuid]['activeuser']['handsetInfo']['currentlocation'] 	= $this->Tracking_model->wayPoints($val['tracking_id'], 1);
				}
			}
		}
	}
	return array_values($info);
}

function getUserCurrentLocation( $deviceId="", $userGuid ="" ){
	$result = array();
	
	if ( empty( $deviceId ) && empty( $userGuid )) {
		return $result;
	}
	$info = $this->getDeviceDetails( '', $deviceId, $userGuid);

	if ( !empty( $info[0] ) ) {
		foreach ( $info[0] as $key=>$val){
			$result['activeuser']['username']  						= (!empty( $val['username'])?$val['username']:'');
			$result['activeuser']['phone']  						= (!empty( $val['phone'])?$val['phone']:'');
			if ( !empty( $val['deviceInfo'] )) {
				$result['activeuser']['deviceInfo']['device_id'] 		= (!empty( $val['deviceInfo']['device_id'] )?$val['deviceInfo']['device_id']:'');
				$result['activeuser']['deviceInfo']['name'] 			= (!empty( $val['deviceInfo']['name'] ) ?$val['deviceInfo']['name']:'');
				$result['activeuser']['deviceInfo']['tracking_id'] 		= (!empty( $val['deviceInfo']['tracking_id'])?$val['deviceInfo']['tracking_id']:'');
				$result['activeuser']['deviceInfo']['currentlocation']	= (!empty( $val['deviceInfo']['currentlocation'])?$val['deviceInfo']['currentlocation']:'');
			}
			if ( !empty( $val['handsetInfo'] )) {
				$result['activeuser']['handsetInfo']['device_id'] 		= (!empty( $val['handsetInfo']['device_id'])?$val['handsetInfo']['device_id']:'');
				$result['activeuser']['handsetInfo']['name'] 			= (!empty( $val['handsetInfo']['name'])?$val['handsetInfo']['name']:'');
				$result['activeuser']['handsetInfo']['tracking_id'] 	= (!empty( $val['handsetInfo']['tracking_id'])?$val['handsetInfo']['tracking_id']:"");
				$result['activeuser']['handsetInfo']['currentlocation'] = (!empty( $val['handsetInfo']['currentlocation'])?$val['handsetInfo']['currentlocation']:"");
			}
		}
	}
	return $result;
}
 
}
?>
