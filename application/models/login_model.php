<?php
Class login_model extends CI_Model
{
function __construct()
    {
        parent::__construct();
    }

 function loginValidation( array $options ){
 	/** Variable creation */
 	$userDetail		=	array();
 		
 	/** Input validation */
 	if(empty($options['password']) || empty($options['username']))
 		return $userDetail;
 		
 		$password		=	md5($options['password']);
 			
 		/** Get user detail information based on email id  **/
 		$userDetail 	=	$this->getLoginUserCredentialDetail( $options );
 			
 		/** Check Password are match **/
 		if(!empty($userDetail) && $password ) {
 			if($userDetail['0']['password']!= $password ) {
 				$userDetail = array();
 			}
 		}
 		
 		return $userDetail;
 }
 
 function loginValidationSupplier( array $options ){
 	/** Variable creation */
 	$userDetail		=	array();
 		
 	/** Input validation */
 	if(empty($options['password']) || empty($options['username']))
 		return $userDetail;
 			
 		$password		=	md5($options['password']);
 
 		/** Get user detail information based on email id  **/
 		$userDetail 	=	$this->getLoginSupplierCredentialDetail( $options );
 
 		/** Check Password are match **/
 		if(!empty($userDetail) && $password ) {
 			if($userDetail['0']['password']!= $password ) {
 				$userDetail = array();
 			}
 		}
 			
 		return $userDetail;
 }
 
 function getLoginUserCredentialDetail( $options ){
 	
 	$username		= (!empty($options['username']))? $options['username']:'';
 	
 	$this -> db -> select('user.username, user.password, user.email, user.guid AS userGuid, role.name AS roleName, role.guid AS userRoleGuid')
 	 
 				->join('user_role AS userrole', 'userrole.user_guid=user.guid')
 				->join('roles AS role', 'role.guid=userrole.role_guid')
				-> where('user.email', $username);
 	$query = $this -> db -> get('user');
 	
 	return $query->result_array();
 }
 
function getLoginSupplierCredentialDetail( $options ){
 	
 	$username		= (!empty($options['username']))? $options['username']:'';
 	
 	$this -> db -> select('user.username, user.password, user.email, user.guid AS userGuid')
				-> where('user.email', $username);
 	$query = $this -> db -> get('user');
 	
 	return $query->result_array();
 }
 
 function loginRedirection( $userRoleGuid ){
 		/** Input validation  */
 		if(empty($userRoleGuid)){
 			return false;
 		}
 			
 		/** variable creation  */
 		$path	= '';
 			
 		switch ($userRoleGuid) {
 
 			case SUPER_ADMIN_ROLE_ID:{
 				$path	= base_url().'index.php/tracking';
 				return $path;
 				break;
 			}
 			case ADMIN_ROLE_ID:{
 				$path	= base_url().'index.php/tracking';
 				return $path;
 				break;
 			}
 			default:{
 				$path = '';
 				return $path;
 				break;
 			}
 		}
 }
 
 function loginActivityEntry( $userGuid, $logId=1, $deviceType='Cloud' ){
 	$result=0;
 	if ( empty( $userGuid )) {
 		return $result;
 	}
 	
 	$loginDate	=	date(DATE_TIME_FORMAT);
 	$activitydata	= array(
				 			'activity_log_id' 	=> $logId,
				 			'user_guid'			=> $userGuid,
				 			'client_date'		=> $loginDate,
				 			'activity_data1'	=> 'Login',
				 			'activity_data2' 	=> 'Login',
				 			'activity_comment' 	=> 'Logged in Successfully',
				 			'device_type' 		=> $deviceType,
				 			'created' 			=> $loginDate,
				 			'created_by' 		=> $userGuid,
 						);
 	$insert	=	$this->db->insert('activity_log', $activitydata);
 	return $insert;
 }
 
 function getSecurityAppDetails($userGuid, $appType='', $appDeviceId=''){
 	
 	$this -> db -> select('user_guid AS userGuid')
			 	-> where('user_guid', $userGuid);
	if( $appType ){
		$this -> db-> where('app_type', $appType);
	}
	if( $appDeviceId ){
		$this -> db->where( 'app_device_id', $appDeviceId );
	}
 				
 	$query = $this -> db -> get('security_app'); 
 	return $query->result_array();
 }
 
 function insertSecurityApp( $activitydata ){
 	$result=0;
 	if ( empty( $activitydata )) {
 		return $result;
 	}
 	$result	=	$this->db->insert('security_app', $activitydata);
 	return $result;
 }
 
}
?>