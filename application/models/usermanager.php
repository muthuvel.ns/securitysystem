<?php
Class Usermanager extends CI_Model
{
	
 function __construct()
 {
   parent::__construct();
   $this->load->model('Tracking_model','',TRUE);
    $this->load->model('User','',TRUE);
 }
 
	function getActiveUserAndDeviceInfo(){
		$data1['user']    =   array();
        $data1['device']    =   array();
            
		$userTrack   =  $this->User->userTrackingDetails();
		/** get user role info */
		$userRole    =  $this->User->getUserRoleInfo();
		
		$inputRole['user_guid']	=	array();
		if (!empty( $userRole)) {
			foreach ( $userRole as $role){
				$inputRole['user_guid'][] =   $role['userGuid'];
			}
		}
		
		if (!empty( $userTrack )) {
			foreach ( $userTrack as $data => $val) {
				$input['user_guid'][] =   $val['user_guid'];
				$input['device_id'][] =   $val['device_id'];
			}
		//	echo '<pre>';print_r($input['user_guid']);
		//	echo '<pre>';print_r(array_unique($input['user_guid']));exit;
			$userGuidList	=	array_merge( array_unique($inputRole['user_guid']), array_unique($input['user_guid']));
			$userDetails    =  $this->User->getUserInfo( $userGuidList );
            $deviceInfo     =  $this->Tracking_model->getdeviceInfo(array_unique($input['device_id']));
         
		}else{
			$userGuidList	= (!empty($inputRole['user_guid'] )? array_unique($inputRole['user_guid']) :'' );
			$userDetails    =  $this->User->getUserInfo( $userGuidList );
			$deviceInfo     =  $this->Tracking_model->getdeviceInfo();
		}
		
          if (!empty( $userDetails)){
                foreach ( $userDetails as $key=>$value) {
                	if ( !empty( $value['device_id']) ) {
                   		$data1['user'][$value['userGuid']] =   $value['username'];
                	}
                }
            }
            
	       if (!empty( $deviceInfo)){
                    foreach ( $deviceInfo as $key1=>$value1) {
                        $data1['device'][$value1['device_id']]   =    $value1['name'];
                    }
           }
		return  $data1;
	}
	
	function getEnableUserAndDeviceInfo( $userGuid = '' ){
		$data1['user']	=   array();
		/** get user tracking details */
		$userTrack    	=  $this->User->userAndDeviceTrackingDetails( $userGuid );
		if (!empty( $userTrack )) {
			foreach ( $userTrack as $data => $val) {
				/** get user profile details*/
				$userProfile    =  $this->User->userAndProfileDetails( $val['userGuid'] );
				$data1['user'][$val['trackId']]['device_id']	=   (!empty($val['device_id'])?$val['device_id']:'');
				$data1['user'][$val['trackId']]['source'] 		=   $val['source'];
				$data1['user'][$val['trackId']]['destination']	=   $val['destination'];
				$data1['user'][$val['trackId']]['user_name']	=   $val['username'];
				$data1['user'][$val['trackId']]['phone']		=   (!empty($userProfile[0]['phone'])?$userProfile[0]['phone']:'');
				$data1['user'][$val['trackId']]['user_email']	=   $val['email'];
				$data1['user'][$val['trackId']]['userGuid']		=   $val['userGuid'];
				$data1['user'][$val['trackId']]['device_name']	=   (!empty($val['devicename'])?$val['devicename']:'');
			}
		}
		return  $data1;
	}
}
?>
