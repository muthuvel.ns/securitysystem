<?php
Class User extends CI_Model {
function __construct()
    {
        parent::__construct();
    }

function userDetails($email='', $guid='') {
   $this -> db -> select('id, username, guid');
   
   if( $email ){
        $this -> db -> where('email', $email);
   }
   if( $guid ){
        $this -> db -> where('guid', $guid);
   }
   $this -> db -> where('deleted', 0);
   $query = $this -> db -> get('user');
     return $query->result_array()  ;
 }
 
 function insertUser($data){
 	$result	=	0;
 	if ( empty($data) ) {
 		return $result;
 	}
 	
 	$result = $this->db->insert('user', $data);
 	return $result;
 	
 }
 
function insertUserProfile($data){
	$result	=	0;
	if ( empty($data) ) {
		return $result;
	}
	
	$result = $this->db->insert('user_profile', $data);
	return $result;
	
 }
 
function updateUser( $userGuid, $data){
	$result	=	0;
	if ( empty($userGuid) || empty($data) ) {
		return $result;
	}
	
	 				$this->db->where('guid', $userGuid);
	 $result	=	$this->db->update('user', $data);
 	return $result;
 }
 
 function updateUserProfile( $userGuid, $data){
 	$result	=	0;
 	if ( empty($userGuid) || empty($data) ) {
 		return $result;
 	}
 
 	$this->db->where('user_guid', $userGuid);
 	$result	=	$this->db->update('user_profile', $data);
 	return $result;
 }
 
 function userTrackingDetails($groupby='', $deviceId='', $userId='', $guid=''){
 	
     $this -> db -> select('ut.id, ut.device_id, ut.user_guid, ut.guid')
    			 ->join('user AS user', 'user.guid = ut.user_guid AND user.deleted=0');
  
	if (!empty($groupby)){
	 $this -> db ->group_by("$groupby");
	}
	if ( !empty($deviceId) ){
	 $this -> db ->where_in('ut.device_id',$deviceId);
	}
	if ( !empty($userId) ){
	 $this -> db ->where_in('ut.user_guid',$userId);
    }
    if ( !empty($guid) ){
    	$this -> db ->where_in('ut.guid',$guid);
    }
    $this -> db ->where('ut.deleted',0);
   
      $query = $this -> db ->get('user_tracking AS ut');
  return $query->result_array();
 }
 
function insertUserTrackingInfo($data){
   return $this->db->insert_batch('user_tracking', $data);
}
 
/**
 * This function used for ser route map
 * here getting device id  so must using join query don't change
 * @param string $whereNot
 * @param string $whereIn
 */
 function getUserInfo($whereNot='', $whereIn='') {
   $this -> db -> select('user.guid AS userGuid, user.username, user.email, profile.phone, profile.photo, profile.device_id,  profile.guid AS profileGuid');
   
   if( $whereNot ){
        $this -> db ->where_not_in('user.guid', $whereNot);
   }
   
   if( $whereIn ){
        $this -> db ->where_in('guid', $whereIn);
   }
   $this->db->join('user_profile AS profile', 'profile.user_guid = user.guid');
   $this -> db ->where('user.deleted', 0);
   $this -> db ->where('profile.deleted', 0);
   $query = $this -> db -> get('user');
  return $query->result_array();
}

function userProfileDetails( $userGuid="", $deviceId=""){
	
	$this -> db -> select('user_guid, phone, photo, device_id, guid');
   
   if( $userGuid ){
        $this -> db ->where_in('user_guid', $userGuid);
   }
   if( $deviceId ){
   	$this -> db ->where_in('device_id', $deviceId);
   }
 
   $this -> db ->where('deleted', 0);
   $query = $this -> db -> get('user_profile');
     
  return $query->result_array();
	
}

function userAndProfileDetails( $userGuid="", $deviceId="", $notInUserGuid=""){

	$this -> db -> select('user.guid AS userGuid, user.username, user.email, profile.phone, profile.photo, profile.device_id, profile.guid AS profileGuid');
	$this->db->join('user_profile AS profile', 'profile.user_guid = user.guid','left');
	
	if( $userGuid ){
		$this -> db ->where_in('user.guid', $userGuid);
	}
	
	if( $deviceId ){
		$this -> db ->where_in('profile.device_id', $deviceId);
	}
	
	if( $notInUserGuid ){
		$this -> db ->where_not_in('user.guid', $notInUserGuid);
	}
	
	$this -> db ->where('user.deleted', 0);
	$query = $this -> db -> get('user');
	return $query->result_array();

}

function userAndDeviceTrackingDetails( $userGuid='', $trackId='' ){
	
	$this -> db -> select('user.username, user.email, user.guid AS userGuid, ut.device_id,ut.guid AS trackId, device.name AS devicename, points.source,points.destination')
				->join('user', 'ut.user_guid = user.guid')
				->join('tracking_info AS device', 'ut.device_id = device.device_id AND device.deleted=0')
				->join('tracking_points AS points', 'ut.guid = points.tracking_id AND points.deleted=0');
	
	if( $userGuid ){
		$this -> db -> where('user.guid', $userGuid);
	}
	
	if( $trackId ){
		$this -> db -> where('points.tracking_id', $trackId);
	}
	
// 	$this -> db -> where('device.deleted', 0);
	$this -> db -> where('ut.deleted', 0);
// 	$this -> db -> group_by('ut.guid');
	$query = $this -> db -> get('user_tracking AS ut');
	return $query->result_array()  ;
	
}

function getUserRoleInfo( $userGuid = ''){
	
	$this -> db -> select('role.name, ur.user_guid AS userGuid')
				->join('user_role as ur', 'role.guid = ur.role_guid');
	if( $userGuid ){
		$this -> db -> where('guid', $userGuid);
	}
	
	$query = $this -> db -> get('roles AS role');
	return $query->result_array()  ;
}

function getUserAndRoleDetails( $userGuid = '', $roleGuid=''){

	$this -> db -> select('user.guid AS userGuid, user.username, user.email, profile.phone, profile.photo, profile.guid AS profileGuid, role.name AS roleName,role.guid AS roleGuid')
				->join('user_profile AS profile', 'profile.user_guid = user.guid','left')
				->join('user_role as ur', 'ur.user_guid = user.guid')
				->join('roles AS role','role.guid = ur.role_guid');
				
	
	if( $userGuid ){
		$this -> db ->where_in('user.guid', $userGuid);
	}
	
	if( $roleGuid ){
		$this -> db ->where_in('ur.role_guid', $roleGuid);
	}
	
	$this -> db ->where('user.deleted', 0);
	$query = $this -> db -> get('user');

	return $query->result_array()  ;
}

function insertUserRole( $data ){
	$result	=	0;
	if ( empty($data) ) {
		return $result;
	}
	
	$result = $this->db->insert('user_role', $data);
	return $result;
}
 
}
?>