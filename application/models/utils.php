<?php
Class Utils extends CI_Model
{

function getGuid( ) {
	$ipaddress	= $_SERVER['SERVER_ADDR'];

	/** Get guid with lower case and md5  */
	$guid	= strtolower( md5( uniqid( (( (double)microtime() * 10000 ) . $ipaddress . 'web'), true ) ) );

	/** Retrun guid */
	return $guid;
}
function getRandomString( $strLength=8, $splSymbol=false ) {
	    $randString             =   'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	    $generateRandomString   =   '';
	    
	    if( $splSymbol ) {
	        $randString .= '!#$%@&*-_=+';
	    }
	    
	    for ($i = 0; $i <$strLength; $i++) {
	        $generateRandomString .= $randString{rand(0, strlen($randString) - 1)};
	    }
	
	 /** return as string **/
	return $generateRandomString;
 }
 
}
?>
