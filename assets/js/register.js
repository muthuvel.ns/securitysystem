function emailavailablility(){

	var email	=	$("#email").val();
	var baseurl	=	$("#baseUrl").val();
	
	// Check email status is valid or not
	var emailstatus	=	is_email(email);
	
	// Check email validation error comes
	var validation_error	=	$( "#email p.msg" ).hasClass( "error-msg" );
	
	// If email status is wrong OR has validation error then RETURN FALSE
	if(!emailstatus || validation_error){
		return false;
	} else {
		// Continue process
	}

  //  var data = $("#email_load").load('/index.php/registration/checkuseremailavailabilty?email='+email);
 // Check email availability
	var jdata	=	$.ajax({
						type: "POST",
						url: baseurl+'index.php/registration/checkuseremailavailabilty?email='+email, 
						dataType: "html",
						async: false
					}).responseText;

	
	//$( "#email p.msg" ).remove();
	
	if( jdata == 1 ){
		// For Email Already exist case : User can't use this email 
		// Append msg to user
		$("#emailid .form-group" ).append( "<p class='msg error-msg'>This email is already taken !</p>" );
		$("#email" ).attr( "current-error", "This email is already taken!" );
		$('#email').removeClass('valid').addClass('error');
		$('#emailid .form-group').removeClass('has-success').addClass('has-error');
		return false;
	} else if( jdata == 2 ){// For Success case : User can use this email 
		// Append msg to user
		$( "#form-item-1 .form-group" ).append( "<p class='msg error-msg success-msg'>Email available !</p>" );
		if( $( "#email" ).attr( "current-error" ) ) {
			// If current error Attribute is exist then remove it.
			$('#email').removeAttr('current-error');
		}
	}
    
    
}

function is_email( data ) { 
	data = data.trim();
	
	if (data.charAt(0) == '_' || data.charAt(0) == '-') /* First character underscore and hyphens not allowed*/
	{
		return false;
	}
	
	var reg = /^((([a-z]|\d|[\_\-`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
    
 	if(reg.test(data) == false) {
  		return false;
 	} else {
		var reg_un = /^([^_]*(_[^_])?)*_?$/;	 /* consecutive underscore not allowed*/
		var reg_hy = /^([^-]*(-[^-])?)*-?$/; 	 /* consecutive hyphen not allowed*/
		if(reg_un.test(data) == false || reg_hy.test(data) == false) {
			return false;
		}
  		return true;
 	}
}
